<?php

namespace Vermal\Database;

/**
 * @method static Database *()
 */
class ElasticSearchIndex extends Connection
{

	public function index()
	{
		$sm = Connection::$searchManager;


		$client = $sm->getClient();

//		$metadatas = $sm->getMetadataFactory()->getAllMetadata();

		die();
		// Delete indexes
		foreach ($metadatas as $metadata) {
			if ($client->getIndex($metadata->index)->exists()) {
				$client->deleteIndex($metadata->index);
			}
		}

		// Recreate indexes and types
		foreach ($metadatas as $metadata) {
			if (!$client->getIndex($metadata->index)->exists()) {
				$client->createIndex($metadata->index);
			}
			$client->createType($metadata);
		}

		return PHP_EOL . 'Succesfully indexed!' . PHP_EOL;
	}

}
