@if($maxPages > 1)
    @php($page = ${$paginator_page_param_name})
    <ul class="pagination">

        <li class="page-item {!! ($page == 1 ? "disabled" : "") !!}">
            @php($prevPage = routerLink($currentRouteName, [$paginator_page_param_name => $page - 1], true, '', true))
            <a {!! ($page == 1 ? "disabled" : "href='" . $prevPage . "'") !!} class="page-link">
                <span aria-hidden="true">&laquo;</span>
            </a>
        </li>

        @php($renderedEllipses = false)
        @for($i = 1; $i <= $maxPages; $i++)

            @php($pageName = routerLink($currentRouteName, [$paginator_page_param_name => $i], true, '', true))

             {{--Render first link--}}
            @if($i < 2)

                <li class="page-item {!! ($page == $i ? "active" : "") !!}">
                    <a class="page-link" href="{!! $pageName !!}">{{ $i }}</a>
                </li>

             {{--Render always 3 links--}}
            @elseif($i == $page || $i == $page - 1  || $i == $page + 1 || ($page == 1 && $i == $page + 2) || ($page == $maxPages && $i == $page - 2))

                <li class="page-item {!! ($page == $i ? "active" : "") !!}">
                    <a class="page-link" href="{!! $pageName !!}">{{ $i }}</a>
                </li>
                @php($renderedEllipses = false)

             {{--Render last link--}}
            @elseif($i == $maxPages)

                <li class="page-item {!! ($page == $i ? "active" : "") !!}">
                    <a class="page-link" href="{!! $pageName !!}">{{ $i }}</a>
                </li>

             {{--Add dots--}}
            @else
                @if(!$renderedEllipses)
                    <li class="page-item disabled">
                        <a class="page-link">...</a>
                    </li>
                    @php($renderedEllipses = true)
                @endif
            @endif
        @endfor

        <li class="page-item {!! ($page == $maxPages ? "disabled" : "") !!}">
            @php($nextPage = routerLink($currentRouteName, [$paginator_page_param_name => $page + 1], true, '', true))
            <a {!! ($page == $maxPages ? "disabled" : "href='" . $nextPage . "'") !!} class="page-link">
                <span aria-hidden="true">&raquo;</span>
            </a>
        </li>

    </ul>
@endif
