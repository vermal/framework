<?php

namespace Vermal\Database\Templates;

use Vermal\Controller;
use Vermal\Router;
use Vermal\View;

class Templates
{

    /**
     * @var string $viewPath
     */
    private static $viewPath = BASE_PATH . '/vendor/vermal/framework/Database/Templates/';

    /**
     * Create paginator
     *
     * @param $paginator
     * @param $limit
     * @param $page_param_name
     */
    public static function paginate($paginator, $limit, $page_param_name)
    {
        View::addTag('paginator', function() use ($paginator, $limit, $page_param_name) {
            $maxPages = ceil($paginator->count() / $limit);

            $params = [
                'maxPages' => $maxPages,
                'limit' => $limit,
                'paginator_page_param_name' => $page_param_name,
                'currentRouteName' => Router::$currentName
            ];
            $params = array_merge($params, Router::$params);

            return View::view(self::$viewPath . 'paginator', $params, true);
        }, false);
    }

}
