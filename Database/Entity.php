<?php

namespace Vermal\Database;


class Entity
{

    /**
     * Get entity
     *
     * @param string $name
     * @param array $args
     * @return mixed
     */
    public static function getEntity($name, $args = [])
    {
        $name = self::getNamespace($name);
        if(count($args) == 0)
            $obj = new $name;
        else {
            $r = new \ReflectionClass($name);
            $obj = $r->newInstanceArgs($args);
        }
        return $obj;
    }

    /**
     * Get class name
     *
     * @param $name
     * @return string
     */
    public static function getNamespace($name)
    {
        return '\Temp\Entities\\' . $name;
    }

}
