<?php

namespace Vermal\Database;


class EntityGenerator
{

    /**
     * Array of clas files
     *
     * @var array $classFiles
     */
    private static $classFiles = [];

    /**
     * Array of classes
     *
     * @var array $classes
     */
    private static $classes = [];

    /**
     * Holds all method clouseres for entites
     *
     * @var array $methods
     */
    public static $methods = [];

    /**
     * Entities
     *
     * @var array $entities
     */
    public static $entities = [];

    /**
     * Generate all classes
     *
     * @param $entites
     * @throws \ReflectionException
     */
    public static function generate($entites)
    {
        self::$entities = $entites;
        foreach ($entites as $namespace => $alias) {
            $class = new \ReflectionClass($namespace);
            $imports = self::getImports($class);
            $properties = self::getProperties($class);
            $methods = self::getMethods($class);

            $classShortname = $class->getShortName();
            if (!isset(self::$classes[$classShortname])) {
                self::$classes[$classShortname] = [
                    'properties' => $properties,
                    'methods' => $methods,
                    'imports' => $imports,
                    'class' => $class,
                ];
            } else {
                self::$classes[$classShortname]['properties'] = array_merge(self::$classes[$classShortname]['properties'], $properties);
                self::$classes[$classShortname]['methods'] = array_merge(self::$classes[$classShortname]['methods'], $methods);
                self::$classes[$classShortname]['imports'] .= $imports;
                self::$classes[$classShortname]['class'] = $class;
            }
        }

        foreach (self::$classes as $class) {
            self::generateClass($class['class'], self::uniqueImports($class['imports']), $class['properties'], $class['methods']);
        }
    }

    /**
     * @param \ReflectionClass $class
     * @param $imports
     * @param $properties
     * @param $methods
     */
    private static function generateClass($class, $imports, $properties, $methods)
    {
        $clazz = "<?php \n\n";

        // Add namespace
        $clazz .= "namespace Temp\Entities;\n\n";

        $clazz .= $imports . "\n\n";

        // Class comment
        $clazz .= str_replace('\\' . $class->getShortName(), '\Temp\Entities\\' . $class->getShortName(),
                $class->getDocComment()) . "\n";

        // Class declaration
        $extends = null;
        if (!empty($class->getParentClass()) && $class->getParentClass()->isAbstract()) {
            $extends = ' extends \\' . $class->getParentClass()->getName();
        }
        $clazz .= "class " . $class->getShortName() . $extends . " { \n\n";

        // Properties
        foreach ($properties as $name => $docBlock) {
            $clazz .= "    " . $docBlock . "\n";
            $clazz .= "    protected $" . $name . ";\n\n";
        }

        // Methods
        foreach ($methods as $name => $method) {
            $clazz .= "    " . $method['docBlock'] . "\n";
            $clazz .= "    {$method['type']} function " . $name . "(";

            // Add params
            end($method['params']);
            $lastElement = key($method['params']);
            foreach ($method['params'] as $paramName => $param) {
                $clazz .= (!empty($param['type']) ? $param['type'] . " " : "");
                $clazz .= "$" . $paramName;
                // Default value
                if (!is_null($param["default"]) && $param['default'] !== 'null')
                    $clazz .= ($param["default"] !== '__defaultIsEmpty' ? " = '" . $param["default"] . "'" : "");
                else {
                    $clazz .= ' = null';
                }
                // Separator
                if ($paramName !== $lastElement)
                    $clazz .= ", ";
            }
            $clazz .= ") {\n";

            // Add Closure
            $clazz .= "        " . $method['closure'];

            $clazz .= "\n    }\n\n";
        }

        $clazz .= "}";

	    // Replace target entity namespace
	    $clazz = str_replace('targetEntity="\\', 'targetEntity="', $clazz);

        // Save class
        $filename = BASE_PATH . '/Temp/Entities/' . $class->getShortName() . '.php';
        file_put_contents($filename, $clazz);
    }

    /**
     * @param \ReflectionClass $class
     * @return array
     */
    private static function getProperties($class)
    {
        $properties = [];
        foreach ($class->getProperties() as $property) {
            $properties[$property->getName()] = $property->getDocComment();
        }

        // Add id on first position not required but is prettier
        if (isset($properties['id'])) {
            $id = $properties['id'];
            unset($properties['id']);
            $properties = array_merge(['id' => $id], $properties);
        }

        return $properties;
    }

    /**
     * @param \ReflectionClass $class
     * @return array
     */
    private static function getMethods($class)
    {
        $methods = [];
        foreach ($class->getMethods() as $method) {
            $methods[$method->getName()] = [
                'closure' => self::getMethodClosure($class, $method),
                'params' => self::getMethodParameters($method),
                'type' => self::getType($method->isPublic(), $method->isProtected(), $method->isPrivate(), $method->isStatic()),
                'docBlock' => $method->getDocComment()
            ];
        }

        return $methods;
    }

    /**
     * @param \ReflectionClass $class
     * @param \ReflectionMethod $method
     * @return string|null
     */
    private static function getMethodClosure($class, $method)
    {
        if (!isset(self::$classFiles[$class->getName()])) {
            self::$classFiles[$class->getName()] = file_get_contents($class->getFileName());
        }
        $search = [];
        $search[] = self::$classFiles[$class->getName()];

        // Traits
        self::getTraits($class, $search);

        // Extended class
        if (!empty($class->getParentClass())) {
            $parent = $class->getParentClass();

            if (!isset(self::$classFiles[$parent->getName()])) {
                self::$classFiles[$parent->getName()] = file_get_contents($parent->getFileName());
            }
            $search[] = self::$classFiles[$parent->getName()];
            self::getTraits($parent, $search);
        }

        // Find method
        foreach ($search as $classFile) {
            $closure = '';
            preg_match_all('~function ' . $method->getName() . '\(.*?(\{(?>[^{}]+|(?1))*\})~s', $classFile, $match);

            // Find right match
            foreach ($match[0] as $key => $group) {
                preg_match('/function ' . $method->getName() . '(\([\s\d\w\$]*\))(\s)*\{/', $group, $m);
                if (!empty($m)) {
                    $closure = $match[1][$key];
                    break;
                }
            }

            // Try to find as single match
            if (empty($closure)) {
                preg_match('~function ' . $method->getName() . '\(.*?.*?(\{(?>[^{}]+|(?1))*\})~s', $classFile, $newMatch);
                if (!empty($newMatch[1])) $closure = $newMatch[1];
            }

            if (!empty($closure)) {
                $closure = trim($closure);
                $closure = trim($closure, '{}');

                // Remove parent
                $closure = preg_replace('/parent::.*?;/', '', $closure);

                // Remove comments
                $find = '/^.*?\/\/.*?$/m';
                while(preg_match($find, $closure)) {
                    $closure = preg_replace($find, '', $closure);
                }

                return trim($closure);
            }
        }
        return null;
    }

    /**
     * Get traits from class
     *
     * @param \ReflectionClass $class
     * @param $search
     */
    private static function getTraits($class, &$search)
    {
        foreach ($class->getTraits() as $trait) {
            if (!isset(self::$classFiles[$trait->getName()])) {
                self::$classFiles[$trait->getName()] = file_get_contents($trait->getFileName());
            }
            $search[] = self::$classFiles[$trait->getName()];
        }
    }

    /**
     * Get parameters for method
     *
     * @param \ReflectionMethod $method
     * @return array
     */
    private static function getMethodParameters($method)
    {
        $params = [];
        foreach ($method->getParameters() as $parameter) {
            // Get type
            $type = null;
            if (!empty($parameter->getType())) {
                $type = $parameter->getType()->getName();
                if (array_key_exists($type, self::$entities)) {
                    $type = self::$entities[$type];
                }
            }

            // try to get default value
            $default = '__defaultIsEmpty';
            try {
                $default = $parameter->getDefaultValue();
                if (!empty($parameter->getDefaultValueConstantName())) {
                    $default = $parameter->getDefaultValueConstantName();
                }
            } catch (\ReflectionException $e) {}

            $params[$parameter->getName()] = [
                'type' => $type,
                'default' => $default
            ];
        }
        return $params;
    }

    /**
     * Get class declaration type as string
     *
     * @param $isPublic
     * @param $isProtected
     * @param $isPrivate
     * @param $isStatic
     * @return string
     */
    private static function getType($isPublic, $isProtected, $isPrivate, $isStatic)
    {
        $type = '';
        if ($isPublic) $type .= 'public';
        else if ($isProtected) $type .= 'protected';
        else if ($isPrivate) $type .= 'private';

        if ($isStatic) $type .= ' static function ';

        return $type;
    }

    /**
     * Get imports from class
     *
     * @param \ReflectionClass $class
     * @return false|string|string[]|null
     */
    private static function getImports($class)
    {
        $classFile = file_get_contents($class->getFileName());
        $imports = $classFile;
        $imports = preg_replace('~abstract class .*\}~s', '', $imports);
        $imports = preg_replace('~class .*\}~s', '', $imports);
        $imports = preg_replace('~trait .*\}~s', '', $imports);
        $imports = preg_replace('~Trait .*\}~s', '', $imports);
//        $imports = preg_replace('~\\/\\*\\*.*\\*\\*\\/~s', '', $imports);
        $imports = str_replace($class->getDocComment(), '', $imports);
        $imports = preg_replace('~namespace.*\;~', '', $imports);
        $imports = preg_replace('~\n~', '', $imports);
        $imports = preg_replace('~\<\?php~', '', $imports);

        // Remove traits from imports
        foreach ($class->getTraits() as $trait) {
            $imports .= self::getImports($trait);
            $imports = str_replace('use ' . $trait->getName() . ';', '', $imports);
        }

        // Remove exended classes from imports
        if (!empty($class->getParentClass())) {
            $imports .= self::getImports($class->getParentClass());
            $imports = str_replace('use ' . $class->getParentClass()->getName() . ';', '', $imports);
        }
        return $imports;
    }

    /**
     * Make imports unique
     *
     * @param $imports
     * @return string
     */
    private static function uniqueImports($imports)
    {
        $imports_a = explode(';', $imports);
        $imports_a = array_filter($imports_a);
        if (!in_array('use Doctrine\ORM\Mapping as ORM', $imports_a) &&
            !in_array('use \Doctrine\ORM\Mapping as ORM', $imports_a)) {
            $imports_a[] = 'use \Doctrine\ORM\Mapping as ORM';
        }
        $imports = implode(";\n", array_unique($imports_a));
        $imports .= ';';

        return $imports;
    }

}
