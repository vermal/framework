<?php

namespace Vermal\Database;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\EventManager;
use Doctrine\ORM\Configuration;
use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\ResolveTargetEntityListener;
use Doctrine\ORM\Tools\Setup;
use Doctrine\Search\DoctrineSearch;
use Doctrine\Search\SearchManager;
use Gedmo\DoctrineExtensions;
use Gedmo\Loggable\LoggableListener;
use Gedmo\Sluggable\SluggableListener;
use Gedmo\Sortable\SortableListener;
use Gedmo\Timestampable\TimestampableListener;
use Gedmo\Translatable\TranslatableListener;
use Gedmo\Tree\TreeListener;
use JMS\Serializer\SerializationContext;
use Vermal\App;
use Whoops\Exception\ErrorException;

class Connection
{

    /** @var EntityManager $em */
    public static $em = null;

    /** @var AnnotationReader  */
    public static $reader;

	/** @var AnnotationDriver */
	public static $driver;
    public static $ENTITIES = [];

	/**
	 * @var Configuration $config
	 */
    private static $config = null;

	/**
	 * @var Configuration $elasticConfig
	 */
	private static $elasticConfig = null;
    private static $paths = [];

	/**
	 * @var SearchManager $searchManager
	 */
    public static $searchManager = null;


    private static $elasticsearch;
    private static $elasticsearchEnabled = false;

    /**
     * @param $dbConfig
     * @return null
     * @throws ErrorException
     * @throws \ReflectionException
     */
    public function boot($dbConfig)
    {
        // Generate entities
        if (count(scandir(BASE_PATH . '/Temp/Entities')) == 2) {
            EntityGenerator::generate($dbConfig['entities']);
        }

        $entities = [];
        foreach ($dbConfig['entities'] as $namespace => $entity) {
            $namespace = explode('\\', $namespace);
            $namespace = array_pop($namespace);
            $namespace = '\Temp\Entities\\' . $namespace;
            $entities[$namespace] = $entity;
            Database::registerEntity($entity, $namespace);
        }
        Database::registerPath(BASE_PATH . '/Temp/Entities');

        // Elasticsearch
		self::$elasticsearch = App::get('elasticsearch');
		self::$elasticsearchEnabled = !empty(self::$elasticsearch) && !empty(self::$elasticsearch['enabled']);

        $dbConfig = $dbConfig['connection'];
        self::config();

        // the connection configuration
        $dbParams = array(
            'host'     => $dbConfig["db_host"],
            'user'     => $dbConfig["db_user"],
            'password' => $dbConfig["db_password"],
            'dbname'   => $dbConfig["db_name"],
            'driver'   => (isset($dbConfig["driver"]) ? $dbConfig["driver"] : 'pdo_mysql'),
            'port'     => (isset($dbConfig["port"]) ? $dbConfig["port"] : 3306),
            'charset'  => (isset($dbConfig["charset"]) ? $dbConfig["charset"] : 'utf8'),
        );

        if (empty($dbParams["host"])) return null;

        try {
            $evm = new EventManager();
			$elasticEvm = new EventManager();
            $rtel = new ResolveTargetEntityListener();

            foreach ($entities as $namespace => $entity) {
                $rtel->addResolveTargetEntity($entity, $namespace, array());
            }
            $evm->addEventListener(Events::loadClassMetadata, $rtel);
			$elasticEvm->addEventListener(Events::loadClassMetadata, $rtel);

            // globally used cache driver, in production use APC or memcached
            $cache = new ArrayCache();
            // standard annotation reader
            $annotationReader = new AnnotationReader();
            self::$reader = $annotationReader;

            $cachedAnnotationReader = new CachedReader(
                $annotationReader,
                $cache
            );

            $driverChain = new MappingDriverChain();

            // Setup paths with namespace
            $annotationDriver = new AnnotationDriver(
                $cachedAnnotationReader,
                self::$paths
            );
            self::$driver = $annotationDriver;

			// ------------------------------------------------
			// Elastic Search
			// ------------------------------------------------
//			if (self::$elasticsearchEnabled) {
//				DoctrineSearch::registerMappingIntoDriverChainORM($driverChain, $annotationReader);
//			}

			// Register gedmo
			DoctrineExtensions::registerMappingIntoDriverChainORM(
				$driverChain, // our metadata driver chain, to hook into
				$cachedAnnotationReader // our cached annotation reader
			);

			$driverChain->addDriver($annotationDriver, 'Temp\Entities');

            // register metadata driver
            self::$config->setMetadataDriverImpl($driverChain);
            self::$elasticConfig->setMetadataDriverImpl($driverChain);
            self::$config->setMetadataCacheImpl($cache);
            self::$elasticConfig->setMetadataCacheImpl($cache);
//			self::$elasticConfig->setEntitySerializer(
//				new JMSSerializer(
//					SerializationContext::create()->setGroups('search')
//				)
//			);
            self::$config->setQueryCacheImpl($cache);

            // sluggable
            $sluggableListener = new SluggableListener();
            // you should set the used annotation reader to listener, to avoid creating new one for mapping drivers
            $sluggableListener->setAnnotationReader($cachedAnnotationReader);
            $evm->addEventSubscriber($sluggableListener);

            // tree
            $treeListener = new TreeListener();
            $treeListener->setAnnotationReader($cachedAnnotationReader);
            $evm->addEventSubscriber($treeListener);

            // loggable, not used in example
            $loggableListener = new LoggableListener();
            $loggableListener->setAnnotationReader($cachedAnnotationReader);

            // timestampable
            $timestampableListener = new TimestampableListener();
            $timestampableListener->setAnnotationReader($cachedAnnotationReader);

            // translatable
            $translatableListener = new TranslatableListener();
            $translatableListener->setTranslatableLocale(App::get('locale'));
            $translatableListener->setDefaultLocale(App::get('locale'));
            $translatableListener->setTranslationFallback(true);
            $translatableListener->setAnnotationReader($cachedAnnotationReader);
            $evm->addEventSubscriber($translatableListener);

            // sortable, not used in example
            $sortableListener = new SortableListener();
            $sortableListener->setAnnotationReader($cachedAnnotationReader);
            $evm->addEventSubscriber($sortableListener);


			// Table Prefix
			$tablePrefix = new TablePrefix(App::get('database.prefix', 'admin_'));
			$evm->addEventListener(Events::loadClassMetadata, $tablePrefix);

			//Get the search manager
//			self::$searchManager = new SearchManager(
//				$annotationDriver,
//				new \Elastica\Client([
//						'username' => self::$elasticsearch['username'],
//						'password' => self::$elasticsearch['password'],
//						'host' => self::$elasticsearch['host'],
//						'port' => self::$elasticsearch['port']
//					]
//				),
//				$elasticEvm,
//				self::$config
//			);

			self::$em = EntityManager::create($dbParams, self::$config, $evm);
			self::$em->getConnection()->connect();

        } Catch(\Exception $e) {
            // echo $e->getMessage();
            throw new ErrorException($e->getMessage());
        }
    }

    public static function config()
    {
        $isDevMode = true;
        self::$config = Setup::createAnnotationMetadataConfiguration(self::$paths, $isDevMode, null, null, false);
        self::$elasticConfig = self::$config;
    }

    public static function registerPath($path) {
        self::$paths[] = $path;
    }

    public static function registerEntity($entity, $namespace) {
        if (self::$config == null) self::config();
        self::$ENTITIES[$entity] = $namespace;
        self::$config->addEntityNamespace($entity, $namespace);
    }

}
