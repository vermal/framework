<?php

namespace Vermal\Database;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Repository\RepositoryFactory;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Vermal\App;
use Vermal\Router;

/**
 * @method static Database *()
 */
class Database extends Connection
{

    /**
     * query
     *
     * @var EntityManager
     */
    public static $query = null;

    /**
     * Self instance
     *
     * @var Database
     */
    private static $_instance = null;

    /**
     * Default entity
     *
     * @var string
     */
    private static $entity = null;

    /**
     * Default entity namespace
     *
     * @var string
     */
    private static $entityNamespace = null;

    /**
     * Alias
     *
     * @var string
     */
    public static $alias = null;

    /**
     * @var bool
     */
    public static $isSelectInQuery = false;

    /**
     * @var null $getResultParameter
     */
    private $getResultParameter = null;

    /**
     * @var bool $isPair
     */
    private $isPair = false;

    /**
     * @var string $locale
     */
    public static $locale;

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository $repo
     */
    private static $repo;

	/**
	 * @var array $parameters
	 */
    private static $parameters = [];

    /**
     * @see Database::build()
     * @method static build()
     *
     * @param $name
     * @param $arguments
     * @return Database
     */
    public static function __callStatic($name, $arguments)
    {
        if ($name == 'Order_') $name = 'Order';
        if (isset(self::$ENTITIES[$name])) {
            array_unshift($arguments, self::$ENTITIES[$name]);
            return call_user_func_array([get_class(), "build"], $arguments);
        }
        return self::$_instance;
    }

    /**
     * @param $name
     * @param string $alias
     * @return self
     */
    public static function Model($name, $alias = null)
    {
        if ($name == 'Order') $name = 'Order_';
        return self::{$name}($alias);
    }

    public static function clear() {
        self::$isSelectInQuery = false;
        self::$alias = null;
        self::$entity = null;
        self::$query;
    }

    /**
     * Build new query
     *
     * @param $alias
     * @param $action
     * @param $entity
     * @return null|Database
     */
    public static function build($entity = null, $alias = null, $action = "select")
    {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }

        if (!empty(self::$ENTITIES) && array_key_exists($entity, self::$ENTITIES)) {
            $entity = self::$ENTITIES[$entity];
        } else {
            if ($entity == null) {
                $entity = self::$entity;
            }
            $entity = self::$entityNamespace . $entity;
        }
        self::$entity = $entity;

        self::$query = self::$em->createQueryBuilder();

        if ($alias == null) {
            $alias = self::createAlias($entity);
        }
        self::$alias = $alias;

        return self::$_instance;
    }


    /**
     * Add translation to object
     *
     * @param $object
     * @param $entity
     * @param string $field
     * @param string $locale
     * @param string $value
     */
    public static function translate(&$object, $entity, $field, $locale, $value)
    {
        // Also save default value
        if (App::get('locale') == $locale) {
            $object->{$field} = $value;
        }
        $tr = Entity::getEntity($entity, [$locale, $field, $value]);
        $object->addTranslation($tr);
        self::save($tr);
    }

    public function join($column, $alias = null, $join = 'leftJoin', $conditionType = null, $condition = null)
    {
        $this->select(self::$entity);
        if ($alias == null) $alias = self::createAlias($column);
        if (strpos($column, '.') === false) {
            $column = self::$alias . "." . $column;
        }
        self::$query = self::$query
            ->{$join}($column, $alias, $conditionType, $condition);
        return $this;
    }

    /**
     * Build in doctrine
     *
     * @param $callback
     * @return $this
     */
    public function doctrine($callback)
    {
        self::$query = call_user_func_array($callback, [
            self::$query
        ]);
        return $this;
    }


    /**
     * @param $entity
     * @return $this
     */
    public function select($entity)
    {
        if (self::$isSelectInQuery) return $this;
        self::$query = self::$query->select(self::$alias)->from($entity, self::$alias);
        self::$isSelectInQuery = true;
        return $this;
    }

	/**
	 * Add select
	 *
	 * @param $field
	 * @return $this
	 */
	public function addSelect($field)
	{
		self::$query = self::$query->addSelect($field);
		return $this;
    }

	public function having($condition)
	{
		self::$query = self::$query->having($condition);
		return $this;
    }

	/**
	 * Group by
	 *
	 * @param $field
	 * @return $this
	 */
	public function groupBy($field)
	{
		self::$query = self::$query->groupBy($field);
		return $this;
	}

    /**
     * Get
     *
     * @param $array
     * @param $debug
     * @return mixed
     */
    public function get($array = false, $debug = false)
    {
        $this->select(self::$entity);
		if ($debug === 'QUERY') {
			return $this->getQuery(self::$query);
		} else if ($debug === 'DQL') {
			return $this->getQuery(self::$query)->getDQL();
		} else if ($debug) {
            return $this->getQuery(self::$query)->getSQL();
        }
        if ($array) {
            $this->getResultParameter = Query::HYDRATE_ARRAY;
        }
        $result = $this->getQuery(self::$query)->getResult($this->getResultParameter);

        // Check if is pairs or normal result
        $pair = $this->isPair;
        $this->isPair = false;
        $this->getResultParameter = null;
        self::clear();
        if (!$pair) {
            return $result;
        } else {
            $newResult = [];
            foreach ($result as $key => $value) {
                $newResult[$value[$pair[0]]] = $value[$pair[1]] . (isset($value[$pair[2]]) ? ' ' . $value[$pair[2]] : '');
            }
            return $newResult;
        }
    }


    /**
     * Fetch pairs
     *
     * @param $key
     * @param $key2
     * @return Database $this
     */
    public function setPair($key, $key2, $key3 = null)
    {
        $keys = [
            self::$alias . '.' . $key,
            self::$alias . '.' . $key2
        ];
        if ($key3 !== null) $keys[] = self::$alias . '.' . $key3;

        self::$query->select($keys)->from(self::$entity, self::$alias);
        self::$isSelectInQuery = true;
        $this->getResultParameter = AbstractQuery::HYDRATE_ARRAY;
        $this->isPair = [
            $key, $key2, $key3
        ];
        return $this;
    }


    /**
     * Find
     *
     * @param null $id
     * @param $array
     * @return mixed
     */
    public function find($id = null, $array = null)
    {
        if ($id !== null) {
            $this->where(self::$alias . ".id", $id);
        }
        return $this->first($array);
    }

    /**
     * Set locale
     *
     * @param $locale
     */
    public function locale($locale)
    {
        if ($locale == App::get('locale')) $locale = '';
        self::$locale = $locale;
        return $this;
    }

    /**
     * Get first result
     *
     * @param $array
     * @throws
     * @return mixed
     */
    public function first($array = null)
    {
        if ($array) {
            $array = Query::HYDRATE_ARRAY;
        }
        $this->select(self::$entity);
        $q = self::$query->setMaxResults(1);
        $r = $this->getQuery($q)->getOneOrNullResult($array);
        self::clear();
        return $r;
    }

    /**
     * Paginator
     *
     * @param $page_param_name
     * @param $from_offset
     * @param $limit
     * @return Paginator
     */
    public function paginate($limit, $from_offset = 0, $page_param_name = "page")
    {
        $this->select(self::$entity);

        $page = (isset(Router::$params[$page_param_name]) ? Router::$params[$page_param_name] : false);

        if ($page) {
            $from = ($page - 1) * $limit;
        } elseif (is_numeric($page_param_name)) {
            $from = ($page_param_name - 1) * $limit;
        } else $from = 0;

        $from = $from + $from_offset;

        $q = self::$query
            ->setFirstResult($from)->setMaxResults($limit);
        $q = $this->getQuery($q);

        $paginator = new Paginator($q, $fetchJoinCollection = true);

        // Add tag to template '@paginator'
        Templates\Templates::paginate($paginator, $limit, $page_param_name);

        self::clear();
        return $paginator;
    }

    public function count($column = null) {
        if ($column == null) $column = "id";
        $q = self::$query
            ->select("count(" . self::$alias . "." . $column . ")")->from(self::$entity, self::$alias);
        $r = $this->getQuery($q)->getSingleScalarResult();
        self::clear();
        return $r;
    }

    /**
     * Build query
     *
     * @param QueryBuilder $q
     *
     * @return EntityManager
     */
    private function getQuery($q)
    {
        $q = $q->getQuery();
        if (!empty(self::$locale)) {
            // set the translation query hint
            $q = $q->setHint(
                \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
                '\\Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
            );
            // locale
            $q = $q->setHint(
                \Gedmo\Translatable\TranslatableListener::HINT_TRANSLATABLE_LOCALE,
                self::$locale // take locale from session or request etc.
            );
            // fallback
            $q = $q->setHint(
                \Gedmo\Translatable\TranslatableListener::HINT_FALLBACK,
                1 // fallback to default values in case if record is not translated
            );
        }
        return $q;
    }

    /**
     * Save
     *
     * @param $entity
     * @throws
     */
    public static function save($entity) {
        self::$em->persist($entity);
        return $entity;
    }


    /**
     * Save and flush
     *
     * @param $entity
     * @return mixed $entity
     * @throws
     */
    public static function saveAndFlush($entity) {
        self::$em->persist($entity);
        self::$em->flush();
        return $entity;
    }

    /**
     * flush
     *
     * @throws
     */
    public static function flush() {
        self::$em->flush();
    }

    /**
     * Delete
     *
     * @param $id
     * @return boolean
     */
    public function delete($id = null)
    {
        if ($id !== null) {
            if (!is_object($id)) {
                $this->where(self::$alias . ".id", $id);
            } else {
                $this->where(self::$alias . ".id", $id->getId());
            }
        }
        $r = self::$query->delete(self::$entity, self::$alias)->getQuery()->getResult();
        self::clear();
        return $r;
    }

    public static function deleteItems($data, $field) {
        foreach ($data->{"get" . ucfirst($field)}() as $row) {
            $data->{"remove" . ucfirst($field)}($row);
        }
        self::$em->persist($data);
        return $data;
    }

    public static function addItems($entity, $data, $dataToAdd, $field) {
        foreach ($dataToAdd as $value) {
            $find = Database::{$entity}()->find($value);
            $data->{"add" . ucfirst($field)}($find);
        }
        return $data;
    }

    /**
     * Where
     *
     * @param $column
     * @param $operator
     * @param null $value
     * @param boolean $or
     * @return $this
     */
    public function where($column, $operator, $value = null, $or = false)
    {
        $this->is_where = true;

        if (!in_array($operator, ["=", ">", "<", ">=", "<=", "!=", "%"])) {
            $value = $operator;
            $operator = "=";
        }

        if ($operator == "!=" && $value == null) {
            $operator = "IS";
            $value = "NOT NULL";
            $null = true;
        } else if ($operator == "=" && $value == null) {
            $operator = "is";
            $value = "NULL";
            $null = true;
        }

        $parameter = str_replace(".", "_", $column);
        if (array_key_exists($parameter, self::$parameters)) {
			self::$parameters[$parameter]++;
        	$parameter .= self::$parameters[$parameter];
		} else {
        	self::$parameters[$parameter] = 1;
		}

        $where = $column . " " . $operator . " :" . $parameter;
        $array = null;

        if (is_array($value)) {
            $where = str_replace(":", "(:", $where . ")");
            $where = str_replace("!", "NOT ", $where);
            $where = str_replace("=", "IN", $where);
            $array = \Doctrine\DBAL\Connection::PARAM_STR_ARRAY;
        } else if ($operator == "%") {
            $value = "%" . $value . "%";
            $where = str_replace("%", "LIKE", $where);
        } elseif (isset($null)) {
            $where = $column . " " . $operator . " " . $value;
        }

        if (!isset($this->is_where)) {
            self::$query = self::$query
                ->where($where);
        } else if ($or) {
            self::$query = self::$query
                ->orWhere($where);
        } else {
            self::$query = self::$query
                ->andWhere($where);
        }

        if (!isset($null)) {
            self::$query = self::$query
                ->setParameter($parameter, $value, $array);
        }

        return $this;
//        return self::$query;
    }

    /**
     * @param $column
     * @param $operator
     * @param null $value
     */
    public function orWhere($column, $operator, $value = null)
    {
        $this->where($column, $operator, $value, true);
        return $this;
    }

    public function rawWhere($key, $parameter, $operator, $value)
    {
        $parameter = str_replace('.', '_', $parameter);
        self::$query = self::$query->andWhere($key . $operator . ':' . $parameter);
        self::$query = self::$query->setParameter($parameter, $value);
        return $this;
    }

	/**
	 * Get query
	 *
	 * @return QueryBuilder
	 */
	public function getRaw()
	{
		return self::$query;
    }

	/**
	 * Set query
	 *
	 * @param $query
	 * @return $this
	 */
	public function setQuery($query)
	{
		self::$query = $query;
		return $this;
    }

    /**
     * Create where group
     */
    public function whereOrGroup()
    {
        $params = [];
        if ( func_num_args() > 0 ) $params = func_get_args();

        if (!isset($this->is_where)) {
            self::$query = self::$query
                ->where(self::$query->expr()->orX($params));
        } else {
            self::$query = self::$query
                ->andWhere(self::$query->expr()->orX($params));
        }
    }

    public function limit($limit, $from = null) {
        if ($from !== null) {
            self::$query = self::$query->setFirstResult($from);
        }
        self::$query = self::$query->setMaxResults($limit);
        return $this;
    }

    /**
     * Order by
     *
     * @param $column
     * @param string $type
     * @return $this
     */
    public function order($column, $type = "ASC")
    {
        self::$query = self::$query->orderBy($column, $type);
        return $this;
    }


    /**
     * Create alias for entity
     *
     * @param $entity
     * @return bool|mixed|string
     */
    private static function createAlias($entity)
    {

        $alias = substr($entity, strrpos($entity, '\\'));
        $alias = str_replace("\\", "", $alias);
        $alias = strtolower($alias);
        $alias = $alias[0];
        return $alias;
    }

    /**
     * Get raw query builder
     */
    public static function raw() {
        return self::$em->createQueryBuilder();
    }

    /**
     * Set entity
     *
     * @param $entity
     */
    public static function setEntity($entity) {
        self::$entity = $entity;
    }

    /**
     * Set namespace
     *
     * @param $namespace
     */
    public static function setNamespace($namespace) {
        self::$entityNamespace = $namespace;
    }

//    /**
//     * Flush at the end
//     */
//    public function __destruct()
//    {
//        // self::$em->flush();
//    }

}
