<?php

namespace Vermal\Database;


Trait MagicAccessor
{

    function __call($method, $params)
    {
        $var = lcfirst(substr($method, 3));

        if (strncasecmp($method, "get", 3) === 0) {
            return $this->{$var};
        }
        if (strncasecmp($method, "set", 3) === 0) {
            $this->{$var} = $params[0];
        }
    }

    public function __get($name)
    {
        return $this->{$name};
    }

    public function __set($name, $value)
    {
        $this->{$name} = $value;
    }

	public function __isset($name) {
		return isset($this->{$name});
	}

}
