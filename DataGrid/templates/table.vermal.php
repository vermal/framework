<form class="data-grid" method="POST" action="{{ $action }}">

    <div class="filter row">
        @if(!empty($filter))
            <div class="col-12">
                <p class="text-muted mb-1">Filter</p>
            </div>
        @endif
        @foreach($filter as $key => $value)
            @if($value['type'] == 'select' && is_array($value['value']))
                <div class="form-group {{ $value['col'] }}">
                    <select title="filter" class="filter select form-control {{ $value['class'] }}" name="filter__{{ $key }}" {!! $value['attr'] !!}>
                        <option value="">{{ $value["label"] }}</option>
                        @foreach($value['value'] as $k => $v)
                            <option value="{{ $k }}"
                                    {!! (isset(${"filter__" . $key}) && ${"filter__" . $key} == $k ? "selected" : "")  !!}>
                                {{ $v }}
                            </option>
                        @endforeach
                    </select>
                </div>
            @elseif($value['type'] == 'input' || $value['type'] == 'search')
                <div class="form-group {{ $value['col'] }}">
                    <input type="{{ $value['inputType'] }}" class="form-control {{ $value['class'] }}" name="filter__{{ $key }}" placeholder="{{ $value['placeholder'] }}"
                           value="{!! (isset(${"filter__" . $key}) ? ${"filter__" . $key} : "")  !!}" autocomplete="off" {!! $value['attr'] !!}>
                </div>
            @endif
        @endforeach
        @if(!empty($filter))
            <div class="col-lg-1 mb-3 mb-lg-0 ml-auto text-right">
                <button class="btn btn-sm btn-warning" id="reset-filter" title="Reset">reset</button>
            </div>
        @endif

        @if(!empty($quickFilter))
            <div class="col-12 mb-3">
                @foreach($quickFilter as $route => $value)
                    <a href="{{ $route }}"
                       class="{{ ($route === currentLink(true) ? 'active' : '') }} mr-2 btn btn-outline-primary btn-sm">
                        {{ $value['label'] }}
                    </a>
                @endforeach
            </div>
        @endif

        <script>

            // TODO: rewrite to vanilla javascript or implement to vermal.js
            $(document).ready(function() {
                try {
                    let url = '{{ $URL }}';
                    if (url.indexOf('?') !== -1) {
                        url = url.substring(0, url.indexOf('?'));
                    }
                    let uri = JSON.parse('{!! json_encode(\Vermal\Router::$queryParams) !!}');
                    if (uri.length < 1) uri = {};

                    // Handle filter on change
                    $('.filter .form-control, tfoot select').on('change', function() {
                        let name = $(this).attr("name");
                        let val = $(this).val();

                        // Check if value is empty
                        if (val === "") {
                            delete uri[name];
                        } else {
                            uri[name] = val;
                        }

                        // if keys are lower than 1 clear uri
                        if (Object.keys(uri).length < 1) uri = "";
                        else uri = "?" + $.param(uri);

                        // Redirect to filter
                        window.location.href = url + uri;
                    });

                    $(document).ready(function() {
                        $('#reset-filter').click(function() {
                            window.location.href = url;
                        });
                    });
                } catch (e) {
                    console.log(e);
                }
            });

        </script>
    </div>

    @if(!empty($bulkActions))
        <div class="row mt-2 mb-2">
            <div class="col-lg-3">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">@__(datagrid.bulkActions)</span>
                    </div>
                    <select name="bulkActions" id="bulkActions" class="form-control" style="border-radius: 0 !important;">
                        @foreach($bulkActions as $key => $action)
                            <option value="{{ $action['action'] }}">{{ $action['label'] }}</option>
                        @endforeach
                    </select>
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-outline-info">
                            <i class='fas fa-dot-circle'></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function() {
                $('#select-all').change(function() {
                    let checked = $(this).prop('checked');
                    $(this).closest('table').find('tbody').find('input[type="checkbox"]').each(function() {
                        $(this).prop('checked', checked);
                    });
                });
            });
        </script>
    @endif

    <div class="global-actions mb-2">
        @foreach($actions as $action)
            @if(!$action['global'])
                @continue
            @endif

            <a href="{!! isset($action['url']['custom']) ? currentLink(true) . $action['url']['custom'] : routerLink($action['url']['link']) !!}"
               class="action {{ $action['class'] }}">
                {!! (!empty($action['icon']) ? '<i class="' . $action['icon'] . '"></i>' : '') !!}
                {!! (isset($action['label']) ? $action['label'] : '') !!}
            </a>
        @endforeach
    </div>

    <div class="table-responsive">
        <table class="{{ $tableClasses }}">

            {{-- Table header --}}
            <thead>
                <tr>
                    @if(!empty($bulkActions))
                        <th>
                            <input type="checkbox" id="select-all">
                        </th>
                    @endif
                    @foreach($columns as $column)
                        <th>{{ $column["label"] }}</th>
                    @endforeach
                    @if(!empty($actions))
                        <th></th>
                    @endif
                </tr>
            </thead>

            {{-- Table body --}}
            <tbody>
                @foreach($data as $row)
                    <tr data-key="{{ $pk }}" data-value="{{ $row->{$pk} }}">
                        @if(!empty($bulkActions))
                            <td><input type="checkbox" name="keys[]" value="{{ $row->{$pk} }}"></td>
                        @endif
                        @foreach($columns as $name => $column)
                            <td class="quickcopy">{!! \Vermal\DataGrid::renderValue($name, $column, $row) !!}</td>
                        @endforeach
                        @if(!empty($actions))
                            <td class="text-right">
                                @foreach($actions as $action)
                                    @if($action['global'])
                                        @continue
                                    @endif

                                    @php($enabled = \Vermal\DataGrid::handleConditions($row, $action['condition']))
                                    @if(!$enabled)
                                        @continue
                                    @endif

                                    @php($params = [])
                                    @if(!empty($action['url']['params']))
                                        @foreach($action['url']['params'] as $k => $key)
                                            @if(strpos($key, '.') === false)
                                                @if(is_int($k))
                                                    @php($params[$key] = $row->{$key})
                                                @else
                                                    @php($params[$k] = $row->{$key})
                                                @endif
                                            @else
                                                @php($exploded = explode('.', $key))
                                                @php($v = $row)
                                                @foreach($exploded as $k)
                                                    @php($v = $v->{$k})
                                                @endforeach
                                                @php($params[$key] = $v)
                                            @endif
                                        @endforeach
                                    @endif

                                    @if(!empty($action['url']['customParams']))
                                        @foreach($action['url']['customParams'] as $k => $value)
                                            @php($params[$k] = $value)
                                        @endforeach
                                    @endif

                                    <a target="{{ $action['target'] }}"
                                       href="{!! isset($action['url']['custom']) ? currentLink(true) . $action['url']['custom'] : routerLink($action['url']['link'], $params) !!}"
                                       class="action {{ $action['class'] }}">
                                        {!! (!empty($action['icon']) ? '<i class="' . $action['icon'] . '"></i>' : '') !!}
                                        {!! (isset($action['label']) ? $action['label'] : '') !!}
                                    </a>
                                @endforeach
                            </td>
                        @endif
                    </tr>
                @endforeach
            </tbody>

            <tfoot>
                <tr>
                    @if(!empty($bulkActions))
                        <td></td>
                    @endif
                    @foreach($columns as $column)
                        <td></td>
                    @endforeach
                    @if(!empty($actions))
                        <td colspan="1" class="text-right">
                            <div class="form-group">
                                <label for="datagrid-results-per-page">Na stranu:</label>
                                <select class="ml-2" name="per_page" id="datagrid-results-per-page">
                                    @foreach($perPageOptions as $key => $option)
                                        @php($key = is_numeric($key) ? $option : $key)
                                        <option value="{{ $key }}" {{ $key == $perPage ? 'selected' : '' }}>{{ $option }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </td>
                    @endif
                </tr>
            </tfoot>

        </table>

        @paginator

    </div>
</form>

<script>
    $(document).ready(function() {
        $('button:not([type="submit"])').click(function(e) {
            e.preventDefault();
        });
    });
</script>
@if(isset($sortable))
    <script>
        $(function() {
            $('table tbody').sortable({
                handle: '.button-sortable',
                helper: fixWidthHelper,
                axis: 'y',
                update: function(e, ui) {
                    let row = $(ui.item);
                    let button = row.find('.button-sortable');
                    $.ajax({
                        url: button.attr('href'),
                        method: 'POST',
                        data: {
                            item_id: row.data('value'),
                            prev_id: row.prev().data('value'),
                            next_id: row.next().data('value')
                        },
                        success: function() {

                        }
                    })
                },
            }).disableSelection();

            function fixWidthHelper(e, ui) {
                ui.children().each(function() {
                    $(this).width($(this).width());
                });
                return ui;
            }
        });
    </script>
@endif

{!! $html !!}
