<?php

namespace Vermal;

use Vermal\Database\Database;
use Whoops\Exception\ErrorException;

/**
 * Class DataGrid
 * @package Vermal\DataGrid
 */
class DataGrid
{

    /**
     * @var array $columns
     */
    private $columns = [];

    /**
     * @var array $actions
     */
    private $actions = [];

	/**
	 * @var string $lastAction
	 */
	private $lastAction = "";

	/**
	 * @var array $bulkActions
	 */
	private $bulkActions = [];

    /**
     * @var Database $data
     */
    private $data = null;

    /**
     * @var integer $perPage
     */
    private $perPage = 25;

	/**
	 * @var array $defaultFilter
	 */
	private $defaultFilter = [];

    /**
     * @var string $last
     */
    private $last = null;

    /**
     * @var null $lastFilter
     */
    private $lastFilter = null;

    /**
     * @var array $filter
     */
    private $filter = [];

	/**
	 * @var array $filter
	 */
	private $quickFilter = [];

	/**
	 * @var array $html
	 */
	private $html = [];

	/**
	 * @var string $primaryKey
	 */
    private $primaryKey = 'id';

	/**
	 * @var string $action
	 */
    private static $action = '';

    /**
     * DataGrid constructor.
     *
     * @param array|object $data
     * @param integer $perPage
	 * @param array $defaultFilter
     */
    public function __construct($data, $perPage = 15, $defaultFilter = [])
    {
        $this->data = $data;
        $this->perPage = $perPage;
        $this->defaultFilter = [];
    }

	/**
	 * Set action
	 *
	 * @param $action
	 */
	public static function setAction($action)
	{
		self::$action = $action;
    }

    /**
     * Add new column
     *
     * @param $key
     * @param $label
     * @return DataGrid $this
     */
    public function addColumn($key, $label)
    {
        $this->columns[$key] = [
            'label' => $label
        ];
        $this->last = $key;
        return $this;
    }

    /**
     * Add new action
     *
     * @param $name
     * @param $url
     * @param $icon
     * @param $label
     * @param $class
	 * @param $condition
	 * @param $target
	 * @return $this
     */
    public function addAction($name, $url, $icon = null, $label = null, $class = null, $condition = [], $target = '_self')
    {
    	if (!is_array($url)) $url = [$url];
        $url = [
            'link' => $url[0] ?? null,
            'params' => $url[1] ?? null,
			'customParams' => $url[2] ?? null,
			'custom' => $url['custom'] ?? null,
        ];
        $this->actions[$name] = [
            'url' => $url,
            'icon' => $icon,
            'label' => $label,
            'class' => $class,
			'condition' => $condition,
			'target' => $target,
			'global' => false
        ];
        $this->lastAction = $name;
        return $this;
    }

	/**
	 * Set action to global
	 */
	public function actionGlobal()
	{
		$this->actions[$this->lastAction]['global'] = true;
    }

    public function addEdit($url, $label = '', $class = 'btn btn-sm btn-primary')
    {
        $this->addAction('edit', $url, 'fas fa-pencil-alt', $label, $class);
    }

    public function addDelete($url, $label = '', $class = 'btn btn-sm btn-warning')
    {
        $this->addAction('delete', $url, 'fas fa-trash', $label, $class);
    }

	public function addSortable($url, $label = '', $class = 'btn btn-sm btn-outline-secondary button-sortable')
	{
		$this->addAction('sort_', $url, 'fas fa-expand-arrows-alt', $label, $class);
		View::setVariable('sortable', true);
	}

	public function addSwitch($url, $key, $label = '')
	{
		$this->addAction('enable', $url, 'fas fa-toggle-on', $label, 'btn btn-sm btn-outline-success', [
			[$key, '=', 1]
		]);
		$this->addAction('disable', $url, 'fas fa-toggle-off', $label, 'btn btn-sm btn-outline-warning', [
			[$key, '=', 0]
		]);
	}

	/**
	 * Add multi action
	 *
	 * @param $action
	 * @param $label
	 * @param array $callback
	 */
	public function addBulkAction($action, $label, $callback)
	{
		$this->bulkActions[] = [
			'action' => $action,
			'label' => $label,
			'callback' => $callback
		];
	}

	/**
	 * Set primary key
	 *
	 * @param string $value
	 */
	public function setPrimaryKey(string $value)
	{
		$this->primaryKey = $value;
	}

    /**
     * Add new filter
     *
     * @param $key
     * @param array $value
     * @param $label
     * @param $col
     * @param $class
	 * @param $attr
     *
     * @return object $this
     */
    public function addFilter($key, $value, $label = null, $col = 'col-md-4', $class = null, $attr = []) {
        $key = str_replace('.', '__', $key);
        $this->lastFilter = $key;
        $this->filter[$key] = [
            'value' => $value,
            'label' => $label,
            'col' => $col,
            'class' => $class,
			'attr' => $this->toAttr($attr),
            'type' => 'select'
        ];
        return $this;
    }

	/**
	 * Add quick new filter
	 *
	 * @param $route
	 * @param $label
	 * @param $col
	 * @param $class
	 * @param $attr
	 *
	 * @return object $this
	 */
	public function addQuickFilter($route, $label = null, $col = 'col-md-4', $class = null, $attr = []) {
		$this->quickFilter[$route] = [
			'label' => $label,
			'col' => $col,
			'class' => $class,
			'attr' => $this->toAttr($attr),
			'type' => 'select'
		];
		return $this;
	}

    /**
     * Add new search filter
     *
     * @param $key
     * @param $type
     * @param $placeholder
     * @param $col
     * @param $class
     * @param $renderType
	 * @param $attr
     *
     * @return object $this
     */
    public function addInputFilter($key, $type, $placeholder = null, $col = 'col-md-4', $class = null, $renderType = 'input', $attr = []) {
        $key = str_replace('.', '__', $key);
        $this->lastFilter = $key;
        $this->filter[$key] = [
            'placeholder' => $placeholder,
            'col' => $col,
            'type' => $renderType,
            'inputType' => $type,
            'class' => $class,
			'attr' => $this->toAttr($attr),
        ];
        return $this;
    }

    /**
     * Add new search filter
     *
     * @param $key
     * @param $placeholder
     * @param $col
	 * @param $class
     */
    public function addSearch($key, $placeholder = null, $col = 'col-md-4', $class = '') {
        $this->addInputFilter($key, 'text', $placeholder, $col, $class, 'search');
    }

    /**
     * Add filter input class
     *
     * @param $class
     *
     * @return $this
     */
    public function filterAddClass($class)
    {
        if ($this->lastFilter !== null) {
            $this->filter[$this->lastFilter]['class'] = $class;
        }
        return $this;
    }

    /**
     * Add filter renderer
     *
     * @param $renderer
     *
     * @return $this
     */
    public function filterRenderer($renderer)
    {
        if ($this->lastFilter !== null) {
            $this->filter[$this->lastFilter]['renderer'] = $renderer;
        }
        return $this;
    }

    /**
     * Set renderer
     *
     * @param $renderer
     */
    public function setRenderer($renderer)
    {
        $this->columns[$this->last]['renderer'] = $renderer;
    }

	/**
	 * Add default filter
	 *
	 * @param $key
	 * @param $value
	 */
	public function addDefaultFilter($key, $value)
	{
		$this->defaultFilter['filter__' . $key] = $value;
    }

	/**
	 * Add custom html content
	 *
	 * @param $html
	 */
	public function addHtml($html)
	{
		$this->html[] = $html;
    }

    /**
     * Run filter function from params
     */
    public function doFilter() {
    	$joins = 0;
    	$filterBy = $this->defaultFilter;
    	$filterBy = array_merge($filterBy, Controller::$params);
        foreach ($filterBy as $key => $value) {
            if (strpos($key, "filter__") !== false) {
                $key = str_replace("filter__", "", $key);
                $filter = $this->filter[$key];
                $key = str_replace("__", ".", $key);
                if (strpos($key, ".") === false) {
                    $key = Database::$alias . "." . $key;
                }
                // Create join
                if (count(explode('.', $key)) > 2) {
                	list(,$join, $field) = explode('.', $key);
                	$alias = 'joined_field_' . $joins;
					$this->data = $this->data->join($join, $alias);
					$key = $alias . '.' . $field;
					$joins++;
				}
                if (isset($filter['renderer'])) {
                    $this->data = call_user_func_array($filter['renderer'], [$value, $this->data]);
                }
                else if ($filter['type'] === 'select') {
					$this->data = $this->data->where($key, $value);
                } else if ($filter['type'] === 'search') {
                    $this->data = $this->data->where($key, '%', $value);
                } else if ($filter['type'] === 'input' && strpos($filter['class'], 'daterange') !== false) {
                    $value = explode(' - ', $value);

                    $value_start = (new \DateTime($value[0]))->setTime(0,0,0,0);
                    $value_end = (new \DateTime($value[1]))->setTime(23,59,59,59);

                    $this->data = $this->data->rawWhere($key, $key . '_start', '>=', $value_start);
                    $this->data = $this->data->rawWhere($key, $key . '_end', '<=', $value_end);
                } else if ($filter['type'] === 'input' && strpos($filter['class'], 'date') !== false) {
                    $value_start = (new \DateTime($value))->setTime(0,0,0,0);
                    $value_end = (new \DateTime($value))->setTime(23,59,59,59);

                    $this->data = $this->data->rawWhere($key, $key . '_start', '>=', $value_start);
                    $this->data = $this->data->rawWhere($key, $key . '_end', '<=', $value_end);
                } else if ($filter['type'] === 'input') {
                    $this->data = $this->data->where($key, $value);
                }
            }
        }
    }

	/**
	 * @return array|object|Database
	 */
	public function getData()
	{
		return $this->data;
    }

	/**
	 * @return array
	 */
	public function getColumns()
	{
		return $this->columns;
    }

    /**
     * Render datagrid
     *
     * @param $paginate
     *
     * @throws ErrorException
     *
     * @return string
     */
    public function build($paginate = true)
    {
        // Build filter
        $this->doFilter();

        // Set filter variables
        foreach ($this->defaultFilter as $key => $value) {
        	if (!isset(Controller::$params[$key]))
        		View::setVariable($key, $value);
		}


        // Set per page
		$perPage = $this->perPage;
        if (!empty(Controller::$params['per_page'])) {
        	$perPage = Controller::$params['per_page'];
        	$this->perPage = $perPage;
        	if ($perPage === 'all') $perPage = 150;
		}

        // Check if paginate or not
        if ($paginate) {
            $this->data = $this->data->paginate($perPage);
        } else {
            $this->data = $this->data->get();
        }

        // Save datagrid for possible actions
        App::sessionSet('datagrid.bulkActions', $this->bulkActions);

        return View::view(BASE_PATH . '/vendor/vermal/framework/DataGrid/templates/table', array_merge([
        	'pk' => $this->primaryKey,
            'data' => $this->data,
            'columns' => $this->columns,
            'actions' => $this->actions,
            'bulkActions' => $this->bulkActions,
            'tableClasses' => 'table',
            'filter' => $this->filter,
			'quickFilter' => $this->quickFilter,
            'URL' => Router::link(Router::$currentName, Router::$params),
			'action' => self::$action,
			'perPage' => $this->perPage,
			'perPageOptions' => [
				10, 15, 20, 25, 30, 35, 40, 50, 'all' => 'All'
			],
			'html' => implode('', $this->html)
        ], Router::$queryParams), true);
    }

    /**
     * Render value
     *
     * @param $nameBase
     * @param $column
     * @param $row
     * @return mixed $value
     */
    public static function renderValue($nameBase, $column, $row)
    {
        $name = explode(".", $nameBase);

        // Check if is set renderer for this column
        if (isset($column['renderer'])) {
            try {
                return call_user_func_array($column['renderer'], [$row, $column]);
            } catch (\Exception $e) {
                return 'There was an error in renderer for column -> ' . $nameBase;
            }
        }

        // Find value by key
        $value = $row;
        foreach ($name as $obj_name) {
            if (!empty($value->{"get" . ucfirst($obj_name)}())) {
                $value = $value->{"get" . ucfirst($obj_name)}();
            } else {
                $data = $value;
                $value = null;
                foreach ($data as $row) {
                    $value .= $row->{"get" . ucfirst($obj_name)}() . ", ";
                }
                $value = rtrim($value, ", ");
            }
        }
        return $value;
    }

	/**
	 * Handle condition
	 *
	 * @param $row
	 * @param $conditions
	 *
	 * @return bool
	 */
	public static function handleConditions($row, $conditions)
	{
		$enabled = true;
		foreach ($conditions as $condition) {
			list($c, $o, $v) = $condition;
			if (isset($row->{$c})) {
				$value = $row->{$c};
			} else if (strpos($c, '.') !== false) {
				$c = explode('.', $c);
				$value = isset($row->{$c[0]}) && isset($row->{$c[0]}->{$c[1]}) ? $row->{$c[0]}->{$c[1]} : null;
				if ($value == null) return true;
			}
			else return true;
			switch ($o) {
				case '=':
					$enabled = $value == $v;
					break;
				case '!=':
					$enabled = $value != $v;
					break;
				case '>':
					$enabled = $value > $v;
					break;
				case '>=':
					$enabled = $value >= $v;
					break;
				case '<':
					$enabled = $value < $v;
					break;
				case '<=':
					$enabled = $value <= $v;
					break;
				case 'IN':
					$enabled = in_array($value, $v);
					break;
			}
		}
		return $enabled;
    }

	/**
	 * Convert PHP array to html attribute string
	 *
	 * @param $attr
	 * @return string
	 */
	private function toAttr($attr)
	{
		if (empty($attr) || !is_array($attr)) return '';
		$attributes = "";
		foreach ($attr as $key => $attribute) {
			$attributes .= $key . '="' . trim($attribute, '"\'') . '" ';
		}
		return rtrim($attributes, " ");
	}

}
