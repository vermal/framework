<?php

namespace Vermal\Traits;


use Vermal\Database\Database;

trait DataGridSort
{

    /**
     * Handle grid sorting
     *
     * @param $model
     * @param  int      $item_id
     * @param  int|NULL $prev_id
     * @param  int|NULL $next_id
     * @return void
     */
    public function sortHelper($model, $item_id, $prev_id, $next_id)
    {
        $item_id = (int)$item_id;
        $prev_id = (int)$prev_id;
        $next_id = (int)$next_id;

		Database::Model($model);
		$alias = Database::$alias;
		Database::clear();

        $item = Database::Model($model)->find($item_id);

        /**
         * 1, Find out order of item BEFORE current item
         */
        if (!$prev_id) {
            $previousItem = NULL;
        } else {
            $previousItem = Database::Model($model)->find($prev_id);
        }

        /**
         * 2, Find out order of item AFTER current item
         */
        if (!$next_id) {
            $nextItem = NULL;
        } else {
            $nextItem = Database::Model($model)->find($next_id);
        }

        /**
         * 3, Find all items that have to be moved one position up
         */
		$itemsToMoveUp = Database::Model($model)
			->where($alias . '.order', '<=', $previousItem ? $previousItem->getOrder() : 0)
			->where($alias . '.order', '>', $item->getOrder())
			->get();

        foreach ($itemsToMoveUp as $t) {
            $t->setOrder($t->getOrder() - 1);
            Database::save($t);
        }

        /**
         * 3, Find all items that have to be moved one position down
         */
		$itemsToMoveDown = Database::Model($model)
			->where($alias . '.order', '>=', $nextItem ? $nextItem->getOrder() : 0)
			->where($alias . '.order', '<', $item->getOrder())
			->get();

        foreach ($itemsToMoveDown as $t) {
            $t->setOrder($t->getOrder() + 1);
			Database::save($t);
        }

        /**
         * Update current item order
         */
        if ($previousItem) {
            $item->setOrder($previousItem->getOrder() + 1);
        } else if ($nextItem) {
            $item->setOrder($nextItem->getOrder() - 1);
        } else {
            $item->setOrder(1);
        }

        Database::saveAndFlush($item);
    }


	/**
	 * Get max value
	 *
	 * @param $model
	 * @return int
	 */
	public function getMaxValue($model)
	{
		$qb = Database::Model($model);
		$alias = Database::$alias;
		$res = $qb->order($alias . '.order', 'DESC')
			->first();
		if (!empty($res))
			return $res->order + 1;
		else return 0;
    }

}
