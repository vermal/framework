<?php

namespace Vermal;

use function GuzzleHttp\Psr7\build_query;
use Whoops\Exception\ErrorException;

class Router
{
    /**
     * @var self $instance
     */
    private static $instance = null;

    /**
     * This static property holds all routes
     *
     * @var array $routes
     */
    private static $routes = [];

    /**
     * Return current route
     *
     * @var string
     */
    public static $route = "";

    /**
     * Return current url
     *
     * @var string
     */
    public static $URL = "";

    /**
     * @var string
     */
    public static $REQUEST = "";

    /**
     * Last route added
     *
     * @var string
     */
    private static $last = null;

    /**
     * @var string
     */
    private static $prefix = null;

    /**
     * @var string
     */
    private static $name = null;

    /**
     * @var array $doNotIncludeInParams
     */
    private static $doNotIncludeInParams = [];

    /**
     * @var string $currentName
     */
    public static $currentName = null;

    /**
     * @var array $params
     */
    public static $params = [];

    /**
     * @var array $defaultValueParams
     */
    public static $defaultValueParams = [];

    /**
     * @var array $queryParams
     */
    public static $queryParams = [];

    /**
     * @var array
     */
    private static $shortcuts = [
        'i' => '(\d+)',
        's' => '([\w\-]+)',
        'paginator' => '(\d+)->1'
    ];

	/**
	 * @var string $prevLink
	 */
    public static $prevLink = '';

	/**
	 * @var string $prevLinkName
	 */
	public static $prevLinkName = '';

    /**
     * Init new self instance
     */
    public static function init() {
        self::$instance = new self();
    }

    /**
     * Asign name to route
     *
     * @param $name
     */
    public function name($name)
    {
        $route = self::$routes[self::$last];
        unset(self::$routes[self::$last]);
        self::$routes[self::$name . $name] = $route;
    }

    /**
     * Add custom shortcut
     * shortcut with default value -> $regex = (val1|val2)->val1;
     *
     * @param $shortcut
     * @param $regex
     */
    public static function addShortcut($shortcut, $regex)
    {
        self::$shortcuts[$shortcut] = $regex;
    }

    /**
     * Get param
     *
     * @param $param
     * @return mixed|null
     */
    public static function getParam($param)
    {
        if (isset(self::$params[$param])) {
            return self::$params[$param];
        }
        return null;
    }

	/**
	 * Get prev link
	 *
	 * @return string|null
	 */
	public static function prevLink()
	{
		if (empty(self::$prevLink))
			self::$prevLink = App::sessionGet('router.prevLink');
		return self::$prevLink;
    }

	/**
	 * Get prev link
	 *
	 * @return string|null
	 */
	public static function prevLinkName()
	{
		if (empty(self::$prevLinkName))
			self::$prevLinkName = App::sessionGet('router.prevLinkName');
		return self::$prevLinkName;
	}

    /**
     * Get link from route name and params
     *
     * @param $route
     * @param array $params
     * @param boolean $absolute
     * @param string $hash
	 * @param bool $withCurrentParams
     *
     * @return string|null
     */
    public static function link($route, $params = [], $absolute = true, $hash = "", $withCurrentParams = false)
    {
        if (!is_array($params)) $params = [];
        $params = array_merge(self::$defaultValueParams, array_filter($params));
        if (!isset(self::$routes[$route])) return null;
        $route = self::$routes[$route];

        // Add current params
        if ($withCurrentParams) {
        	$params = array_merge(Controller::$params, $params);
		}

        // Remove wildcard params
        foreach ($params as $key => $param) {
        	if (strpos($key, 'wildcard_') !== false) {
        		unset($params[$key]);
			}
		}

        $link = "";
        $paramKeys = [];
        foreach ($route['params'] as $key => $param) {
            if (!isset($param['real']))
                $paramKeys[] = $param['name'];

            if (isset($param['real']) && $param['real'] == false) {
                $link .= $param['pattern'] . '/';
            } else if (isset($params[$param['name']])) {
                // Chcek if param has default
                if (isset($param['default']) && $param['default'] !== $params[$param['name']]) {
                    $link .= $params[$param['name']] . '/';
                }
            }
        }

        // Remove default params
        foreach (self::$defaultValueParams as $param => $value) {
            if (!in_array($param, $paramKeys) && isset($params[$param])) {
                unset($params[$param]);
            }
        }

        // add parameter as query string
        $query = [];
		$wildCard = "";
        foreach ($params as $param => $value) {
			if ($param === '*') {
				$wildCard .= '/' . $value;
				continue;
			}
            if (!in_array($param, $paramKeys)) {
                $query[$param] = $value;
            }
        }
        $query = build_query($query);

        // Add absolute path
        if ($absolute) {
            $link = self::$URL . $link;
        }

        // Cut slash at the end
        return rtrim($link, '/') . $wildCard . (!empty($query) ? '?' . $query : null) . (!empty($hash) ? '#' . $hash : null);
    }

    /**
     * Redirict to specific route or defined location
     *
     * @param $route
     * @param array $params
     * @param string $hash
     *
     * @return mixed
     */
    public static function redirect($route, $params = [], $hash = "")
    {
        if (!is_array($params)) $params = [];
        $params = array_merge(self::$defaultValueParams, array_filter($params));
        if (!isset(self::$routes[$route])) return null;
        $route = self::link($route, $params, true, $hash);

        header('Location: ' . $route, true);
        die();
    }

	/**
	 * Redirect to url
	 *
	 * @param $url
	 */
	public static function redirectToURL($url)
	{
		header('Location: ' . $url, true);
		die();
    }

    /**
     * Create prefixed routes
     *
     * @param $prefix
     * @param $callback
     * @param string $name
     * @param array $doNotIncludeInParams
     */
    public static function prefix($prefix, $callback, $name = '', $doNotIncludeInParams = [])
    {
        self::$prefix = $prefix;
        self::$name = $name;
        self::$doNotIncludeInParams = $doNotIncludeInParams;
        call_user_func($callback);
        self::$prefix = null;
        self::$name = null;
        self::$doNotIncludeInParams = [];
    }

    /**
     * Create route
     *
     * @param $route
     * @param $action
     * @param array $method
     * @return Router
     */
    public static function route($route, $action, $method = ["POST", "GET"])
    {
        $prefix = self::$prefix;

        // Remove leading slash from route
        $route = ltrim($route, "/");

        // Remove end slash if route is empty but prefix isn't
        if (empty($route) && !empty(self::$prefix)) {
            $prefix = rtrim(self::$prefix, '/');
        }

        $explodedRoute = explode("/", $prefix . $route);
        $params = [];
        $pattern = "";

        // create route
        foreach ($explodedRoute as $key => $r) {
            if (preg_match('~\*~', $r)) {
                $hasWildCard = true;
                $pattern = substr($pattern, 0, -1);
                $r = str_replace('*', '([\/][a-zA-Z-0-9]+)*', $r);
                $params[] = [
                    'pattern' => $r,
                    'name' => 'wildcard_' . uniqid(),
                    'default' => '',
                    'wildcard' => '*'
                ];
            } else if (strpos($r, '}?') !== false) {
                $r = self::dynamic($r, $params, true);
                $pattern = substr($pattern, 0, -1);
                $dyn = true;
            } else if (strpos($r, '}') !== false) {
                $r = self::dynamic($r, $params, false);
                $pattern = substr($pattern, 0, -1);
                $dyn = true;
            } else {
                $params[] = [
                    'pattern' => $r,
                    'real' => false
                ];
            }
            $pattern .= ($key == 0 && !isset($dyn) ? '/' : '') . $r . '/';
        }

        // Create pattern
        $pattern = substr($pattern, 0, -1) . '/?';
        if (isset($hasWildCard)) {
            $pattern = substr($pattern, 0, -2);
        }
        $pattern = '~^' . str_replace('/', '\/', $pattern) . '$~';

        // Save data to static property
        $name = uniqid();
        self::$routes[$name] = [
            'route' => $route,
            'pattern' => $pattern,
            'params' => $params,
            'action' => $action,
            'method' => $method,
            'doNotIncludeInParams' => self::$doNotIncludeInParams
        ];
        // Set last added
        self::$last = $name;

        // return self instance
        if (self::$instance == null) {
            self::init();
        }
        return self::$instance;
    }

    /**
     * Helper for route with GET method
     *
     * @param $route
     * @param $action
     * @return self
     */
    public static function get($route, $action)
    {
        return self::route($route, $action, ['GET']);
    }

    /**
     * Helper for route with POST method
     *
     * @param $route
     * @param $action
     * @return self
     */
    public static function post($route, $action)
    {
        return self::route($route, $action, ['POST']);
    }

    /**
     * Helper for route with PUT method
     *
     * @param $route
     * @param $action
     * @return self
     */
    public static function put($route, $action)
    {
        return self::route($route, $action, ['PUT']);
    }

    /**
     * Helper for route with DELETE method
     *
     * @param $route
     * @param $action
     * @return self
     */
    public static function delete($route, $action)
    {
        return self::route($route, $action, ['DELETE']);
    }

    /**
     * Helper for route with GET, POST, PUT, DELETE method
     *
     * @param $route
     * @param $action
     * @return self
     */
    public static function any($route, $action)
    {
        return self::route($route, $action, ['GET', 'POST', 'PUT', 'DELETE']);
    }

    /**
     * Create all routes for resource (CRUD)
     *
     * @param $route
     * @param $controller
     * @param string $name
     * @param string $shortcut
     */
    public static function resource($route, $controller, $name = null, $shortcut = 's')
    {
        self::$name = self::$name . $name;

        self::get($route . '/{page::paginator}?', $controller . "@index")->name('index');
        self::get($route . "/create", $controller . "@create")->name('create');
        self::post($route, $controller . "@store")->name('store');
        self::get($route . "/detail/{id::" . $shortcut . "}", $controller . "@show")->name('show');
        self::get($route . "/{id::" . $shortcut . "}/edit", $controller . "@edit")->name('edit');
        self::put($route . "/{id::" . $shortcut . "}", $controller . "@update")->name('update');
        self::delete($route . "/{id::" . $shortcut . "}", $controller . "@destroy")->name('destroy');
        self::get($route . "/{id::" . $shortcut . "}/delete", $controller . "@destroy")->name('destroy_');

        self::$name = str_replace($name, '', self::$name);
    }


    /**
     * Handle dynamic parameter in route
     *
     * @param string $route
     * @param array $params
     * @param boolean $optional
     * @return mixed
     */
    private static function dynamic($route, &$params, $optional = false)
    {
        $shortcut = self::rules($route);

        $name = str_replace('{', '', $route);
        if (!$optional) $name = str_replace('}', '', $name);
        else $name = str_replace('}?', '', $name);

        if (strpos($name, '::')) {
            $name = substr($name, 0, strpos($name, "::"));
        }

        if (array_search($name, array_column($params, 'name')) === false) {

            $pattern = str_replace('(', '(/', $shortcut['shortcut']);
            $pattern = str_replace('|', '|/', $pattern);

            // If is optional add ? at the end of pattern
            if ($optional) {
                $pattern .= '?';
            }

            $params[] = [
                'name' => $name,
                'pattern' => $pattern,
                'default' => $shortcut['default']
            ];
        } else {
            die('Parameter with name ' . $name . ' has been already defined');
        }
        return $pattern;
    }

    /**
     * Dynamic route rules
     *
     * @param $route
     * @return mixed
     */
    private static function rules($route)
    {
        if (preg_match('~::(.*?)}~', $route, $match)) {
            list(, $shortcut) = $match;
            if (isset(self::$shortcuts[$shortcut])) {
                // Try to get default value from shortcut
                $default = '';
                $shortcut = self::$shortcuts[$shortcut];
                if (preg_match('~->(.*?)$~', $shortcut, $match)) {
                    $default = $match[1];
                    $shortcut = str_replace($match[0], '', $shortcut);
                }
                // Return shortcut and default value
                return [
                    'shortcut' => $shortcut,
                    'default' => $default
                ];
            }
        }
        return [
            'shortcut' => self::$shortcuts['s'],
            'default' => ''
        ];
    }

    /**
     * Execute router
     *
     * @param $ROOT
     * @return boolean
     */
    public static function execute($ROOT)
    {
        // Get query string
        $request = explode('?', $_SERVER['REQUEST_URI']);
        $queryParams = [];
        if (isset($request[1])) {
            $queryParams = $request[1];
            parse_str($queryParams, $queryParams);
            self::$queryParams = $queryParams;
        }

        // Modify request
        if (self::$REQUEST !== '/') {
            $request = rtrim(self::$REQUEST, '/');
        } else {
            $request = self::$REQUEST;
        }

        // Find route
        foreach (self::$routes as $routeKey => $route) {

            $matchMethod = in_array($_SERVER['REQUEST_METHOD'], $route['method']) || (isset($_POST["_method"]) && in_array($_POST["_method"], $route['method']));
            if (preg_match($route['pattern'], $request, $match) && $matchMethod) {

                // Set current page name
                self::$currentName = $routeKey;

                // Default variables
                $explodedRequest = explode('/', ltrim($request, '/'));
                $routeParams = $route['params'];
                $params = [];

                // Match request params with params in array - static params
                foreach ($explodedRequest as $key => $value) {
                    foreach ($routeParams as $k => $routeParam) {
                        // Go to the next request part
                        if (isset($routeParam['real']) && $routeParam['pattern'] == $value) {
                            unset($routeParams[$k]);
                            unset($explodedRequest[$key]);
                            break;
                        }
                    }
                }

                // Match request params with params in array - dynamic params
                foreach ($explodedRequest as $key => $value) {
                    foreach ($routeParams as $k => $routeParam) {
                        if ($k >= $key && ($k - $key) < 2) {
                            if (preg_match('~' . $routeParam['pattern'] . '~', '/' . $value, $match)) {
                                $params[$routeParam['name']] = '';
                                if (isset($routeParam['wildcard'])) {
                                    $start = false;
                                    foreach ($explodedRequest as $kk => $v) {
                                        if ($kk == $key) $start = true;
                                        if ($start) {
                                            $params[$routeParam['name']] .= $v . '/';
                                        }
                                    }
                                    break(2);
                                } else if (preg_match('~.*?\|.*?$~', $routeParam['pattern'], $m)) {
                                    $m[0] = str_replace('(', '', $m[0]);
                                    $m[0] = str_replace(')', '', $m[0]);
                                    $m[0] = str_replace('/', '', $m[0]);
                                    $m[0] = str_replace('?', '', $m[0]);
                                    $patternParams = explode('|', $m[0]);
                                    if (($v = array_search($value, $patternParams)) !== false) {
                                        self::$defaultValueParams[$routeParam['name']] = $patternParams[$v];
                                        $params[$routeParam['name']] = $patternParams[$v];
                                        unset($routeParams[$k]);
                                    }
                                } else {
                                    $params[$routeParam['name']] = $value;
                                    unset($routeParams[$k]);
                                }
                            }
                        }
                    }
                }

                // Last try to assign params - only with default values
                foreach ($routeParams as $k => $routeParam) {
                    if (!isset($routeParam['default']) || empty($routeParam['default'])) continue;
                    $params[$routeParam['name']] = $routeParam['default'];
                    self::$defaultValueParams[$routeParam['name']] = $routeParam['default'];
                }

                // Resort params to default order
                $resortedParams = [];
                foreach ($route['params'] as $k => $routeParam) {
                    if (isset($routeParam['name']) && isset($params[$routeParam['name']])) {
                        $resortedParams[$routeParam['name']] = $params[$routeParam['name']];
                    }
                }

                // Merge with query params
                $resortedParams = array_merge($resortedParams, $queryParams);
                self::$params = $resortedParams;

                // Check if can redirect to some defaults
                $link =  self::link($routeKey, $resortedParams, false, '', true);
                if (empty($routeParam['wildcard'])) {
					if (trim($request, '/') !== explode('?', $link)[0]) {
						header('Location: ' . self::$URL . $link);
					}
				}
				self::prevLink();
                self::prevLinkName();
				if (in_array('GET', $route['method'])) {
					App::sessionSet('router.prevLink', self::$URL . $link);
					App::sessionSet('router.prevLinkName', $routeKey);
				}

                // Setup default route and url
                self::$route = $route;

                // If is post request
                if (in_array('POST', $route['method']) || in_array('PUT', $route['method']) || in_array('DELETE', $route['method'])) {
                    $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
                    if (strpos($contentType, 'application/json') !== false) {
                        $post = json_decode(file_get_contents('php://input'), false);
                    } else {
                        $post = self::array_to_object($_POST);
                    }
                    array_unshift($resortedParams, $post);
                }

                // Check if there are some unwanted params
                $params = $resortedParams;
                foreach ($resortedParams as $key => $value) {
                    if (in_array($key, $route['doNotIncludeInParams']) && !is_numeric($key)) {
                        unset($resortedParams[$key]);
                    }
                }

                // Call action
                if (is_callable($route['action'])) {
                    call_user_func_array($route['action'], $resortedParams);
                } else if (strpos($route['action'], '@') !== false) {
                    // call controller
                    list($controller, $method) = explode('@', $route['action']);

                    if (strpos($controller, '\\') === false) {
                        $controller = 'App\\Controllers\\' . $controller;
                    }

                    // Add properties to controller
                    foreach ($params as $key => $value) {
                        Controller::$params[$key] = $value;
                    }

                    // init new controller
                    $controller = new $controller;

                    // Call method
                    call_user_func_array([$controller, $method], $resortedParams);
                }
                return 'SUCCESS';
            }
        }
        if (App::get('debug')) {
            throw new ErrorException("Route not found");
        } else {
            http_response_code(404);
            View::view(BASE_PATH . '/vendor/vermal/framework/Router/error/error', [
                'code' => 404,
                'msg' => 'Túto stránku sme nenašli'
            ]);
        }
    }

    /**
     * Check if connection is secure
     *
     * @return bool
     */
    private static function isSecure()
    {
        if (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') {
            return true;
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' ||
            !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on' ||
            ((isset($_SERVER['HTTP_CF_VISITOR']) && $visitor = json_decode($_SERVER['HTTP_CF_VISITOR'])) && $visitor->scheme == 'https') )  {
            return true;
        }
        return false;
    }

    /**
     * Generate URL
     *
     * @param $ROOT
     */
    public static function generateURL($ROOT)
    {
		$ROOT = str_replace('..', '', $ROOT);
		$baseLink = (self::isSecure() ? "https" : "http");
        $baseLink .= "://" . $_SERVER['SERVER_NAME'];
        $baseLink .= ($_SERVER['SERVER_PORT'] !== '80' && $_SERVER['SERVER_PORT'] !== '443' ? ':' . $_SERVER['SERVER_PORT'] : '');

        $request = $_SERVER['REQUEST_URI'];
        $request = explode('?', $request)[0];

        // Explode absolute path and request to array
        $rootArray = array_reverse(explode('/', trim($ROOT, '/')));
        $requestArray = array_reverse(explode('/', trim($request, '/')));

        $i = 0;
        $baseRequest = '';
        foreach ($requestArray as $key => $value) {
            if (!isset($rootArray[$i]) || $value !== $rootArray[$i]) {
                continue;
            } else {
                $baseRequest .= "/" . $value;
                $i++;
            }
        }
        $baseRequest = rtrim(implode('/', array_reverse(explode('/', $baseRequest))), '/');

        // Remove string from request
        $request = str_replace($baseRequest, '', $request);
        $request = str_replace('//', '/', $request);

        $URL = $baseLink . '/';
        if (!empty($baseRequest)) {
            $URL .= str_replace('//', '/', $baseRequest . '/');
        }
        $URL = rtrim($URL, '/') . '/';

        self::$URL = $URL;
        define('URL', $URL);
        self::$REQUEST = $request;
    }

    /**
     * Array to object
     *
     * @param $array
     * @return \stdClass
     */
    private static function array_to_object($array) {
        $obj = new \stdClass;
        foreach($array as $k => $v) {
            if(strlen($k)) {
                if(is_array($v)) {
                    $obj->{$k} = self::array_to_object($v); //RECURSION
                } else {
                    $obj->{$k} = $v;
                }
            }
        }
        return $obj;
    }

    /**
     * Searches for $needle in the multidimensional array $haystack.
     *
     * @param mixed $needle The item to search for
     * @param array $haystack The array to search
     * @return array|bool The indices of $needle in $haystack across the
     *  various dimensions. FALSE if $needle was not found.
     */
    private static function recursive_array_search($needle,$haystack) {
        foreach($haystack as $key=>$value) {
            if($needle===$value) {
                return array($key);
            } else if (is_array($value) && $subkey = self::recursive_array_search($needle,$value)) {
                array_unshift($subkey, $key);
                return $subkey;
            }
        }
    }
}
