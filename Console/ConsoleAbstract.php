<?php

namespace Vermal\Console;


abstract class ConsoleAbstract
{

    /**
     * @param $msg
     */
    public function printMsg($msg)
    {
        echo "\n" . trim($msg) . "\n\n";
        exit();
    }

    /**
     * Get template
     *
     * @param $name
     * @param $variables
     * @return bool|string
     */
    protected function getTemplate($name, $variables = [])
    {
        $template = file_get_contents(__DIR__ . '/templates/' . $name . '.txt');
        foreach ($variables as $key => $value) {
            $template = str_replace("{{ " . $key . " }}", $value, $template);
        }
        return $template;
    }

    protected function saveFile($path, $data)
    {
        $path = explode('/', $path);

        $dir = BASE_PATH . '/App/';
        $fileName = null;
        foreach ($path as $key => $value) {
            // Set filename and break because this isn't directory
            if ($key == (count($path) - 1)) {
                $fileName = $value;
                break;
            }

            $dir .= $value . '/';
            if (!is_dir($dir)) {
                if (!mkdir($dir)) {
                    $this->printMsg('Failed to create folder "' . $dir . '"');
                }
            }
        }

        file_put_contents($dir . $fileName . '.php', $data);

        $this->printMsg('Controller was created successfully: ' . $dir . $fileName . '.php');
    }

}