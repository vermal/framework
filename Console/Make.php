<?php

namespace Vermal\Console;


use Vermal\App;
use Vermal\Database\EntityGenerator;

class Make extends ConsoleAbstract
{

    /**
     * Create controller
     *
     * @param $nameParam
     * @param $anotherArg
     */
    public function controller($nameParam, $anotherArg)
    {
        $params = $this->parseNameAndNamespace($nameParam);
        $namespace = $params['namespace'];
        $name = $params['name'];

        if ($anotherArg !== 'resource') $template = 'Controller';
        else $template = 'ControllerResource';

        $tempalte = $this->getTemplate($template, [
            "NAMESPACE" => $namespace,
            "NAME" => $name
        ]);

        $this->saveFile('Controllers/' . str_replace('.', '/', $nameParam), $tempalte);
    }

    /**
     * Create model
     *
     * @param $nameParam
     * @param $anotherArg
     */
    public function model($nameParam, $anotherArg)
    {
        $params = $this->parseNameAndNamespace($nameParam);
        $namespace = $params['namespace'];
        $name = $params['name'];

        $tempalte = $this->getTemplate('Model', [
            "NAMESPACE" => $namespace,
            "NAME" => $name,
            "TABLE_NAME" => strtolower($name)
        ]);

        $this->saveFile('Models/' . str_replace('.', '/', $nameParam), $tempalte);
    }

    /**
     * Get name and namespace from args
     *
     * @param $nameParam
     * @return array
     */
    private function parseNameAndNamespace($nameParam)
    {
        $nameParam = str_replace('.', '\\', $nameParam);
        $name = '\\' . $nameParam;

        $namespace = "";
        $namespaceExploded = explode("\\", $nameParam);
        foreach ($namespaceExploded as $key => $value) {
            if ($key == (count($namespaceExploded) - 1)) break;
            $namespace .= $value . '\\';
        }
        if ($namespace !== '') {
            $namespace = '\\' . rtrim($namespace, '\\');
        }

        $name = substr($name, strrpos($name, '\\') + 1);

        return [
            "name" => $name,
            "namespace" => $namespace
        ];
    }

    /**
     * Generate entities
     */
    public function entities($nameParam, $anotherArg)
    {
        foreach(glob(BASE_PATH . '/Temp/Entities/*') as $file) {
            if (is_file($file))
                unlink($file);
        }
        EntityGenerator::generate(App::get('database')['entities']);
    }

}
