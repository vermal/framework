<?php

namespace Vermal\Console;

use Vermal\Database\ElasticSearchIndex;

class Console extends ConsoleAbstract {

    private $args = [];
    private $command = '';
    private static $commands = [];
    private static $help = "
Vermal:
    - make controller (controller) [resource] -> Generate Controller
    - make model (model) -> Generate model
    - make entities -> Genereate entites

Doctrine:
    - doctrine:update
    - doctrine:update:dump
    - doctrine:elasticsearch:index
    
All:";

    /**
     * Console constructor.
     */
    public function __construct()
    {
        unset($_SERVER['argv'][0]);
        $this->command = $_SERVER['argv'][1];
        unset($_SERVER['argv'][1]);

        $this->args = array_values($_SERVER['argv']);

        $this->defaultCommands();
        $this->executeCommand();
    }

    /**
     * Default commands
     */
    private function defaultCommands()
    {
        /**
         * Make command
         * create new controller
         */
        self::registerCommand('make', function($make = null, $name = null, $anotherArg = null) {
            if ($make == null) $this->printMsg('Specify make command, please!');
            if ($name == null && $make !== 'entities') $this->printMsg('Specify name of the ' . $make . ', please!');

            if (!in_array($make, ['controller', 'model', 'entities'])) {
                $this->printMsg('This make command doesn\'t exist!');
            }

            (new Make())->{$make}($name, $anotherArg);
        });

        self::registerCommand('help', function() {
            $this->printMsg(self::$help);
        });

        /**
         * Doctrine commands
         */
        self::registerCommand('doctrine:update', function() {
            echo shell_exec('php ' . BASE_PATH . '/vendor/bin/doctrine orm:schema-tool:update --force');
        });
        self::registerCommand('doctrine:update:dump', function() {
            echo shell_exec('php ' . BASE_PATH . '/vendor/bin/doctrine orm:schema:update --dump-sql');
        });
		self::registerCommand('doctrine:elasticsearch:index', function() {
			echo (new ElasticSearchIndex())->index();
		});
    }

    /**
     * Register new command
     *
     * @param $name
     * @param $callback
     */
    public static function registerCommand($name, $callback)
    {
        self::$help .= "
    - {$name}";
        self::$commands[$name] = $callback;
    }


    /**
     * Execute command and check if exist
     */
    private function executeCommand()
    {
        if (isset(self::$commands[$this->command])) {
            call_user_func_array(self::$commands[$this->command], $this->args);
        } else {
            $this->printMsg("Command \"" . $this->command . "\" does not exit!");
        }
    }

}
