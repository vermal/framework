<?php

namespace Vermal;

use Vermal\Console\Console;
use Vermal\Database\Connection;
use Whoops\Exception\ErrorException;

class App
{
    use Config;

    /**
     * The Vermal framework version.
     *
     * @var string
     */
    const VERSION = '2.1.0';

    /**
     * Application's dependencies & config
     *
     * @var array
     */
    protected static $map = [];

    /**
     * Create a new Vermal application instance.
     *
     * @param string $basePath
     * @param bool $debug
     *
     * @throws
     *
     * @return void
     */
    public function __construct($basePath, $debug = false)
    {
        $this->getAppConfig($basePath);
        $this->setBasePath($basePath);

        self::bind('debug', $debug);
        if ($debug && !defined('IS_SCRIPT')) {
            $whoops = self::bind('whoops', new \Whoops\Run);
            self::bind('whoops.handler', $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler));
            $whoops->register();
        } else {
            set_exception_handler(function($exception) {
                echo "
                    <div style='position: relative; top: 50%; transform: translateY(-50%); text-align: center; font-family: \"Open sans\", sans-serif''>
                        <h1>500</h1>
                        <h3>Na stránke nastala chyba, skúste to znovu neskôr prosím. Už na tom pracujeme.</h3>  
                        <a style='display: inline-block; padding: 8px 20px; background: #2a2a2a; color: #fff; text-decoration: none; border-radius: 3px' href='" . routerLink('home') . "'>Domov</a>  
                    </div>
                ";
                self::log($exception->getMessage());
                die();
            });

        }

        // Start session
        $this->startSession();

        // Autoload Classes
        spl_autoload_register(function ($className) {
            $load = $this->loadClass($className);
            if (!empty($load)) {
                include_once $this->loadClass($className);
            }
        });

        // Handle session timeouts
        $this->handleSessionTimeout(self::get('sessionTimeout'));

        // Define url
        if (!defined('IS_SCRIPT'))
            Router::generateURL(BASE_PATH);

        $this->boot();

        // Define fallback URL from config
        if (defined('IS_SCRIPT'))
            define('URL', 'CONFIG_URL');

        $this->bootProviders();
        $this->afterBootProviders();

        // Create database connection
        if (!empty(self::get('database')['connection'])) {
            (new Connection())->boot(self::get('database'));
        }

        // Execute Router
        if (!defined('IS_SCRIPT')) {
            Router::execute($basePath);
        } else if (!defined('IS_DOCTRINE')) {
            new Console();
        }
    }

    private function startSession()
    {
        session_start();
    }

    /**
     * Get the version number of the application.
     *
     * @return string
     */
    public function version()
    {
        return static::VERSION;
    }

    /**
     * Set the base path for the application.
     *
     * @param  string  $basePath
     * @return $this
     */
    public function setBasePath($basePath)
    {
        $basePath = rtrim($basePath, '\/');
        self::bind("BASE_PATH", $basePath);
        define("BASE_PATH", $basePath);
        return $this;
    }

    /**
     * PSR-4 class auto load
     *
     * @param $className
     * @return string
     */
    public function loadClass($className)
    {
        $className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
        $file = self::get('BASE_PATH') . DIRECTORY_SEPARATOR . $className . ".php";
        if (file_exists($file)) {
            return $file;
        }
        return null;
    }

    /**
     * Handle session timeout
     *
     * @param integer $time
     */
    private function handleSessionTimeout($time)
    {
        $created = self::sessionGet('CREATED');
        if (empty(self::sessionGet('CREATED'))) {
            self::sessionSet('CREATED', time());
        } else if (time() - $created > $time) {
            // change session ID for the current session and invalidate old session ID
            session_regenerate_id(true);
            // update creation time
            self::sessionSet('CREATED', time());
            self::bind("SESSION_EXPIRED", true);
        }
    }

    /**
     * Set session value
     *
     * @param $key
     * @param $value
     * @return mixed|null
     */
    public static function sessionSet($key, $value)
    {
        if (!isset($_SESSION['VERMAL']))
            $_SESSION['VERMAL'] = [];
        $_SESSION['VERMAL'][$key] = $value;
        return $value;
    }

    /**
     * Session key add value to array
     *
     * @param $key
     * @param $value
     */
    public static function sessionAdd($key, $value)
    {
        if (!isset($_SESSION['VERMAL'][$key]))
            $_SESSION['VERMAL'][$key] = [];
        $_SESSION['VERMAL'][$key][] = $value;
    }

    /**
     * Get session value
     *
     * @param $key
     * @return mixed|null
     */
    public static function sessionGet($key)
    {
        if (isset($_SESSION['VERMAL'][$key]))
            return $_SESSION['VERMAL'][$key];
        return null;
    }

    /**
     * Binder
     *
     * @param $key
     * @param $value
     *
     * @return mixed $value
     */
    public static function bind($key, $value)
    {
        static::$map[$key] = $value;
        return $value;
    }

    /**
     * Binder
     *
     * @param $key
     * @param $value
     *
     * @return mixed $value
     */
    public static function merge($key, $value)
    {
        if (!isset(static::$map[$key]))
            static::$map[$key] = [];
        if (is_array($value))
            static::$map[$key] = array_merge(static::$map[$key], $value);
        return $value;
    }

    /**
     * Resolve app key
     *
     * @param $key
     * @param $default
     * @return mixed
     */
    public static function get($key, $default = null)
    {
        if (isset(static::$map[$key])) {
            return static::$map[$key];
        } else return $default;
    }

    /**
     * @param $message
     */
    public static function log($message)
    {
        $dir = BASE_PATH . '/public/log/';
        if (!is_dir($dir)) {
            mkdir($dir, 0775);
        }
        $filename = $dir . 'system.log';

        file_put_contents($filename, '[' . date('Y-m-d H:i:s') . ']: ' . $message, FILE_APPEND);
    }

}
