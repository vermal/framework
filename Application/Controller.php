<?php

namespace Vermal;

use Vermal\Admin\Defaults\Mailer;
use Vermal\Form\Form;

/**
 * Class Controller
 * @package Vermal\Config
 */
abstract class Controller
{

	use Translator;

    public static $params = [];

	public static $langFileDir = BASE_PATH . '/public/lang/';
    public $langData;
    public static $langDataStatic;
    public static $alerts = [];

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        foreach (self::$params as $key => $param) {
            $this->{$key} = $param;
        }

        if (empty(self::$alerts)) {
			self::$alerts = $this->alerts();
		}
        View::setVariable('alerts', self::$alerts);

		// Localisation files
		$this->langData = $this->getLocalizationFile(self::$langFileDir);
		$this->langData = array_merge_recursive($this->langData, $this->getLocalizationFile(BASE_PATH . '/App/Admin/tr/'));
		foreach (App::get('localization-files') as $path) {
			$this->langData = array_merge_recursive($this->langData, $this->getLocalizationFile($path));
		}
		self::$langDataStatic = $this->langData;

		// Set form locale
		Form::setLocale($this->locale);

		// Add translation tag @__(key)
		View::addTag('__', function ($match) {
			return $this->_($match[1]);
		}, true, false);

		// Presenter
		View::setVariable('presenter', $this);
    }

    /**
     * Save alert to session
     *
     * @param $msg
     * @param string $type
     * @param array $aditional
     */
    public function alert($msg, $type = 'info', $aditional = [])
    {
        self::alertStatic($msg, $type, $aditional);
    }

	/**
	 * Save alert to session
	 *
	 * @param $msg
	 * @param string $type
	 * @param array $aditional
	 */
	public static function alertStatic($msg, $type = 'info', $aditional = [])
	{
		$array = [
			'msg' => $msg,
			'type' => $type
		];
		$array = array_merge($array, $aditional);

		if (!self::isAjax()) {
			$_SESSION['alert'][] = $array;
		} else {
			$array['notify'] = $array['msg'];
			echo json_encode($array);
		}
	}

    /**
     * Return alert from session if alert is not set in session return empty array
     *
     * @return array
     */
    public function alerts()
    {
        if (isset($_SESSION['alert'])) {
            $alerts = $_SESSION['alert'];
			unset($_SESSION['alert']);
            return $alerts;
        }
        return [];
    }

    /**
     * Controller destructor.
     */
    public function __destruct()
    {
    }

    /**
     * Check if request is ajax
     *
     * @return bool
     */
    public static function isAjax()
    {
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Convert string to slug
     *
     * @param $text
     * @return null|string|string[]
     */
    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

	/**
	 * Get new instance of mailer
	 *
	 * @param $settings
	 * @return Mailer
	 */
	protected static function mailer($settings) {
		$mailer = new Mailer(
			$settings->mailer_username,
			$settings->mailer_password,
			$settings->mailer_host,
			$settings->mailer_port,
			$settings->mailer_encryption
		);
		$mailer->from($settings->mailer_username, $settings->mailer_name);
		return $mailer;
	}

	/**
	 * Get param
	 *
	 * @param $key
	 * @return |null
	 */
	public function getParam($key)
	{
		if (!empty(Router::$params[$key]))
			return Router::$params[$key];
		else return null;
	}

	/**
	 * Set param
	 *
	 * @param $key
	 * @param $value
	 * @return |null
	 */
	public function setParam($key, $value)
	{
		Router::$params[$key] = $value;
	}

}
