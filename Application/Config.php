<?php

namespace Vermal;


trait Config
{

    /**
     * Load appliaction config from app.php
     *
     * @param $basePath
     */
    protected function getAppConfig($basePath)
    {
        self::$map = include ($basePath . '/config/app.php');
    }

    /**
     * Register all service providers.
     *
     * @return void
     */
    protected function bootProviders()
    {
        foreach (self::get('providers') as $key => $provider) {
            $provider = new $provider;
            if (method_exists($provider, "boot")) {
                $provider->boot();
            }
        }
    }

    /**
     * Register all service providers.
     *
     * @return void
     */
    protected function afterBootProviders()
    {
        foreach (self::get('providers') as $key => $provider) {
            $provider = new $provider;
            if (method_exists($provider, "afterBoot")) {
                $provider->afterBoot();
            }
        }
    }

    /**
     * Boot application
     */
    protected function boot()
    {
        date_default_timezone_set(self::get('timezone'));
        setlocale(LC_ALL, self::get('locale'));

		self::$map = array_merge_recursive(self::$map, include self::get('BASE_PATH') . '/config/' . self::get('env') . '.php');
//        if (self::get('env') !== 'development') {
//            self::$map = array_merge_recursive(self::$map, include self::get('BASE_PATH') . '/config/production.php');
//        } else {
//            self::$map = array_merge_recursive(self::$map, include self::get('BASE_PATH') . '/config/development.php');
//        }

        define('DEBUG', boolval(self::get('debug')));
        define('CONFIG_URL', self::get('url'));

        // Set view variables
        View::setVariable('locale', self::get('locale'));
        View::setVariable('debug', self::get('debug'));
        View::setVariable('timezone', self::get('timezone'));

        // Load routes
        require_once self::get('BASE_PATH') . '/routes/web.php';
    }

}
