<?php

namespace Vermal;


use Symfony\Component\Yaml\Yaml;

trait Translator
{

	protected function getLocalizationFile($dir)
	{
		$langFile = $dir . $this->locale . '.yaml';
		if (!file_exists($langFile)) {
			$langFile = $dir . 'en.yaml';
		}
		if (file_exists($langFile))
			return Yaml::parse(file_get_contents($langFile));
		return [];
	}

	/**
	 * Get value from language file
	 *
	 * @param $key
	 * @return string
	 */
	protected function _($key)
	{
		try {
			$value = self::arrayGetByKey(self::$langDataStatic, $key . '.');
			return $value;
		} catch (\Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * Get value from language file
	 *
	 * @param $key
	 * @return array|mixed
	 */
	public static function __($key)
	{
		try {
			$value = self::arrayGetByKey(self::$langDataStatic, $key . '.');
			return $value;
		} catch (\Exception $e) {
			return $e->getMessage();
		}
	}

	private static function arrayGetByKey(&$array, $key, $value = null)
	{
		list($index, $key) = explode('.', $key, 2);
		if (!isset($array[$index])) throw new \Exception("No such key: " . $index);

		if (strlen($key) > 0)
			return self::arrayGetByKey($array[$index], $key, $value);

		$old = $array[$index];
		if ($value !== null) $array[$index] = $value;
		return $old;
	}

}
