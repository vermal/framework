<?php

/**
 * Generate link
 *
 * @param $route
 * @param array $params
 * @param boolean $absolute
 * @param string $hash
 * @param bool $withCurrentParams
 *
 * @return string|null
 */
function routerLink($route, $params = [], $absolute = true, $hash = "", $withCurrentParams = false) {
    return Vermal\Router::link($route, $params, $absolute, $hash, $withCurrentParams);
}

/**
 * Get current link
 *
 * @param $withParams
 * @return string
 */
function currentLink($withParams = false) {
	$params = [];
	if (!empty($withParams))
		$params = array_merge(\Vermal\Router::$params, \Vermal\Router::$queryParams);
	return routerLink(\Vermal\Router::$currentName, $params, true, '', true);
}

/**
 * Set active class
 *
 * @param $route
 * @param $class
 * @return string
 */
function addActiveClassRouter($route, $class = 'active')
{
	if (!is_array($route)) $route = [$route];
	$routes = [];
	foreach ($route as $r) {
		$routes[] = routerLink($r, [], true, '', true);
	}
	if (in_array(currentLink(true), $routes)) {
		return $class;
	} else return null;
}

/**
 * Render view
 *
 * @param $view
 * @param array $variables
 *
 * @throws \Whoops\Exception\ErrorException
 */
function view($view, $variables = []) {
    \Vermal\View::view($view, $variables);
}

/**
 * Create slug from text
 *
 * @param $text
 * @return false|string|string[]|null
 */
function slugify($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);
    // trim
    $text = trim($text, '-');
    // remove duplicate -
    $text = preg_replace('~-+~', '-', $text);
    // lowercase
    $text = strtolower($text);
    if (empty($text)) {
        return 'n-a';
    }
    return $text;
}


/**
 * printf array values
 *
 * @param $format
 * @param $arr
 * @return mixed
 */
function printfArray($format, $arr)
{
    return call_user_func_array('sprintf', array_merge((array)$format, $arr));
}


/**
 * Add filter callback
 *
 * @param $key
 * @param $callback
 * @param integer $priority
 */
function add_filter($key, $callback, $priority = 0) {
    $filters = \Vermal\App::get('filter_' . $key);
    if (empty($filters)) $filters = [];
    $filters[] = [
        'callback' => $callback,
        'priority' => $priority
    ];
    \Vermal\App::bind('filter_' . $key, $filters);
}

/**
 * Apply filters
 *
 * @param $key
 * @param $return
 * @param $param
 * @return mixed
 */
function apply_filters($key, $return, $param = null) {
    $filters = \Vermal\App::get('filter_' . $key);
    if (empty($filters)) return $return;
    usort($filters, function($a, $b) {
        return $a['priority'] <=> $b['priority'];
    });
    foreach ($filters as $filter) {
        if (is_callable($filter['callback'])) {
            $return_ = call_user_func_array($filter['callback'], [$return, $param]);
            if (!empty($return) || !empty($return_))
            	$return = $return_;
        }
    }
    return $return;
}


/**
 * Add action callback
 *
 * @param $key
 * @param $callback
 */
function add_action($key, $callback) {
    $actions = \Vermal\App::get('action_' . $key);
    if (empty($actions)) $actions = [];
    $actions[] = $callback;
    \Vermal\App::bind('action_' . $key, $actions);
}

/**
 * Do action
 *
 * @param $key
 * @param $param
 */
function do_action($key, $param = []) {
    $actions = \Vermal\App::get('action_' . $key);
    if (!empty($actions)){
        foreach ($actions as $action) {
            if (is_callable($action)) {
                call_user_func_array($action, [$param]);
            }
        }
    }
}

/**
 * Generate all the possible combinations among a set of nested arrays.
 *
 * @param array $data  The entrypoint array container.
 * @param array $all   The final container (used internally).
 * @param array $group The sub container (used internally).
 * @param mixed $value   The value to append (used internally).
 * @param int   $i     The key index (used internally).
 *
 * @return array
 */
function generate_combinations(array $data, array &$all = array(), array $group = array(), $value = null, $i = 0)
{
	$keys = array_keys($data);
	if (isset($value) === true) {
		array_push($group, $value);
	}

	if ($i >= count($data)) {
		array_push($all, $group);
	} else {
		$currentKey     = $keys[$i];
		$currentElement = $data[$currentKey];
		foreach ($currentElement as $val) {
			generate_combinations($data, $all, $group, $val, $i + 1);
		}
	}

	return $all;
}

/**
 * Get value from app
 *
 * @param $key
 * @param null $default
 *
 * @return mixed
 */
function appGet($key, $default = null) {
	return \Vermal\App::get($key, $default);
}

/**
 * Set value for app
 *
 * @param $key
 * @param $value
 */
function appBind($key, $value) {
	\Vermal\App::bind($key, $value);
}


/**
 * Get operators
 *
 * @param bool $all
 * @return array
 */
function getOperators($all = false) {
	$operators = [
		'=' => '=', '!=' => '!=', '>' => '>', '>=' => '>=', '<' => '<', '<=' => '<='
	];
	if ($all) {
		$operators = array_merge($operators, [
			'range' => 'RANGE',
			'in' => 'IN',
			'notin' => 'NOT IN'
		]);
	}
	return $operators;
}

//define( 'MINUTE_IN_SECONDS', 60 );
//define( 'HOUR_IN_SECONDS', 60 * MINUTE_IN_SECONDS );
//define( 'DAY_IN_SECONDS', 24 * HOUR_IN_SECONDS );
//define( 'WEEK_IN_SECONDS', 7 * DAY_IN_SECONDS );
//define( 'MONTH_IN_SECONDS', 30 * DAY_IN_SECONDS );
//define( 'YEAR_IN_SECONDS', 365 * DAY_IN_SECONDS );
//
///**
// * Human time diff
// *
// * @param $from
// * @param int $to
// * @return string
// */
//function human_time_diff( $from, $to = 0 ) {
//	if ( empty( $to ) ) {
//		$to = time();
//	}
//
//	$diff = (int) abs( $to - $from );
//
//	if ( $diff < MINUTE_IN_SECONDS ) {
//		$secs = $diff;
//		if ( $secs <= 1 ) {
//			$secs = 1;
//		}
//		/* translators: Time difference between two dates, in seconds. %s: Number of seconds. */
//		$since = sprintf( _n( '%s second', '%s seconds', $secs ), $secs );
//	} elseif ( $diff < HOUR_IN_SECONDS && $diff >= MINUTE_IN_SECONDS ) {
//		$mins = round( $diff / MINUTE_IN_SECONDS );
//		if ( $mins <= 1 ) {
//			$mins = 1;
//		}
//		/* translators: Time difference between two dates, in minutes (min=minute). %s: Number of minutes. */
//		$since = sprintf( _n( '%s min', '%s mins', $mins ), $mins );
//	} elseif ( $diff < DAY_IN_SECONDS && $diff >= HOUR_IN_SECONDS ) {
//		$hours = round( $diff / HOUR_IN_SECONDS );
//		if ( $hours <= 1 ) {
//			$hours = 1;
//		}
//		/* translators: Time difference between two dates, in hours. %s: Number of hours. */
//		$since = sprintf( _n( '%s hour', '%s hours', $hours ), $hours );
//	} elseif ( $diff < WEEK_IN_SECONDS && $diff >= DAY_IN_SECONDS ) {
//		$days = round( $diff / DAY_IN_SECONDS );
//		if ( $days <= 1 ) {
//			$days = 1;
//		}
//		/* translators: Time difference between two dates, in days. %s: Number of days. */
//		$since = sprintf( _n( '%s day', '%s days', $days ), $days );
//	} elseif ( $diff < MONTH_IN_SECONDS && $diff >= WEEK_IN_SECONDS ) {
//		$weeks = round( $diff / WEEK_IN_SECONDS );
//		if ( $weeks <= 1 ) {
//			$weeks = 1;
//		}
//		/* translators: Time difference between two dates, in weeks. %s: Number of weeks. */
//		$since = sprintf( _n( '%s week', '%s weeks', $weeks ), $weeks );
//	} elseif ( $diff < YEAR_IN_SECONDS && $diff >= MONTH_IN_SECONDS ) {
//		$months = round( $diff / MONTH_IN_SECONDS );
//		if ( $months <= 1 ) {
//			$months = 1;
//		}
//		/* translators: Time difference between two dates, in months. %s: Number of months. */
//		$since = sprintf( _n( '%s month', '%s months', $months ), $months );
//	} elseif ( $diff >= YEAR_IN_SECONDS ) {
//		$years = round( $diff / YEAR_IN_SECONDS );
//		if ( $years <= 1 ) {
//			$years = 1;
//		}
//		/* translators: Time difference between two dates, in years. %s: Number of years. */
//		$since = sprintf( _n( '%s year', '%s years', $years ), $years );
//	}
//
//	return $since;
//}
//
//
///**
// * @param array $values
// * @param $number
// * @param string $domain
// */
//function _translation( $values, $number, $domain = 'default' )
//{
//	foreach ($values as $value => $translation) {
//		if ($value >= $number && $value <= $number) {
//
//		}
//	}
//}
