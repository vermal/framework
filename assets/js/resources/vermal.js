import { sprintf } from './sprintf';
import { button, notify } from "./vermal-helpers";

window.vermalButton = button;
window.vermalNotify = notify;

let activeAjax = null;

$(document).ready(function() {

    let validate = true;
    $('form.validate').on('submit validate', function(e, data) {
        let form = $(this);

        let error = false;
        let lastErrorFormGroup = null;

        $(this).find('.form-group').each(function() {
            let error_ = validateControl($(this), data);
            if (error_) {
                error = true;
                lastErrorFormGroup = $(this);
            }
        });

        if (error) {
            form.trigger('fields-error', {
                'fGroup': lastErrorFormGroup
            });
            return false;
        }

        // Run ajax if requested
        if (data !== undefined && data.btn !== undefined && !error) {
            $('.form-group .message').hide();
            adminAjax(data.btn, form);
        }
    });

    $('form .form-control, form .custom-control-input.custom-control-input-switch').each(function() {
        let form = $(this).closest('form');
        let fGroup = $(this).closest('.form-group');
        handleToggle($(this), form, fGroup);
    });
});

let validateControl = function(fGroup, data) {
    if (typeof fGroup.find('input').attr('name') === "undefined") return false;

    // Check if validation is only for specific field
    if (typeof data !== 'undefined' && data.fGroup !== undefined) {
        fGroup = data.fGroup;
    }

    // Get rules for current control
    let rules = fGroup.data("rules");
    if (typeof rules === "undefined") return false;

    // Get control
    let control = fGroup.find('.form-control, .custom-control-input.custom-control-input-switch');

    // Set control val
    let controlVal = control.val();

    // If is checkbox switch
    if (control.hasClass('custom-control-input-switch')) {
        if (!control.is(':checked')) {
            controlVal = '';
        }
    }

    let error = false;

    // Iterate over rules and display errors
    $.each(rules, function(rule, value) {
        let err = $rules[rule].call(null, value, controlVal);

        // If error was found
        if (err !== true) {
            let errClass = err[0];
            err = sprintf(tr[err[0]], value);

            // Remove old and add new message
            fGroup.find('.' + errClass).remove();
            fGroup.append("<span class='invalid-feedback d-block " + errClass + "'>" + err + "</span>");

            error = true;
        } else {
            // Remove old and add new message
            fGroup.find('.invalid-feedback').remove();
        }
    });

    return error;
};

/**
 * Available rules
 */
let $rules = {
    "email": function(value, control) {
        return validateEmail(value, control);
    },
    "int": function(value, control) {
        return validateInt(value, control);
    },
    "required": function(value, control) {
        return validateRequired(value, control);
    },
    "min": function(value, control) {
        return validateMin(value, control);
    },
    "max": function(value, control) {
        return validateMax(value, control);
    }
};


/**
 * Validate email
 *
 * @param value
 * @param control
 * @returns {*}
 */
function validateEmail(value, control) {
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(control)) {
        return true;
    } else {
        return ["EMAIL", ""];
    }
}

/**
 * Validate integer|float
 *
 * @param value
 * @param control
 * @returns {*}
 */
function validateInt(value, control) {
    if (control.match(/^-?\d*(\.\d+)?$/)) {
        return true;
    } else {
        return ["INT", ""];
    }
}

/**
 * Validate if is empty
 *
 * @param value
 * @param control
 * @returns {*}
 */
function validateRequired(value, control) {
    if (control !== "" && control !== "0") {
        return true;
    } else {
        return ["REQUIRED", ""];
    }
}

/**
 * Min strlen
 *
 * @param value
 * @param control
 * @returns {*}
 */
function validateMin(value, control) {
    if (control.length >= value) {
        return true;
    } else {
        return ["MIN", value];
    }
}

/**
 * Max strlen
 *
 * @param value
 * @param control
 * @returns {*}
 */
function validateMax(value, control) {
    if (control.length <= value) {
        return true;
    } else {
        return ["MAX", value];
    }
}


/**
 * Vermal simple ajax using jQuery
 */
$('form.ajax button').click(function(e) {
    e.preventDefault();
    if ($(this).hasClass('disable-auto-ajax'))
        return;
    let form = $(this).closest('form');
    if (form.hasClass('validate')) {
        form.trigger('validate', {btn: $(this)});
    } else {
        adminAjax($(this), form);
    }
});

$('form .form-control, form .custom-control-input.custom-control-input-switch').on('change', function() {
    let form = $(this).closest('form');
    let fGroup = $(this).closest('.form-group');
    handleToggle($(this), form, fGroup);
    if (form.hasClass('validate')) {
        form.trigger('validate', {fGroup: fGroup});
    }
});

function handleToggle(input, form, fGroup)
{
    let data = fGroup.data('toggles');
    let val = input.val();
    if (input.attr('type') === 'checkbox') {
        val = input.prop('checked') ? 1 : 0;
    }
    if (typeof data !== "undefined") {
        $.each(data, function(i, item) {
            if (!getResult(val, item.operator, item.value)) {
                $('#' + item.id).hide();
            } else $('#' + item.id).show();
        });
    }
}

/**
 * Get result of operation
 *
 * @param value1
 * @param operator
 * @param value2
 * @returns {boolean}
 */
function getResult(value1, operator, value2)
{
    if (operator === '=')
        return value1 == value2;
    else if (operator === '!=')
        return value1 != value2;
    else if (operator === '>')
        return value1 > value2;
    else if (operator === '>=')
        return value1 >= value2;
    else if (operator === '<')
        return value1 < value2;
    else if (operator === '<=')
        return value1 <= value2;
    else if (operator === 'IN' && Array.isArray(value2))
        return value2.indexOf(value1) > -1;
    else return false;
}

/**
 * Ajax
 *
 * @param btn
 * @param form
 */
let adminAjax = function(btn, form)
{
    let formName = form.attr('id');

    // Trigger start method for current form
    $('#' + formName).trigger('start', {form: form, button: btn});

    // Attributes from form object
    let action = form.attr('action');
    let method = form.attr('method');

    // Create formData
    let formData = new FormData();
    let serializedForm = form.serializeArray();
    for (let i = 0; i < serializedForm.length; i++) {
        formData.append(serializedForm[i].name, serializedForm[i].value);
    }

    // Append file inputs to formData
    $('input[type="file"]').each(function() {
        let inputName = $(this).attr('name');
        $.each($(this)[0].files, function(i, file) {
            formData.append(inputName + "[]", file);
        });
    });

    // Append button name value to formData
    if (btn !== null) {
        formData.append(btn.attr('name'), btn.val());
    }

    // Abort previous ajax
    if (activeAjax !== null) activeAjax.abort();
    // Call ajax
    activeAjax = $.ajax({
        url: action,
        type: method,
        data: formData,
        processData: false,
        contentType: false,
        success(response) {
            // Trigger succesfull method for current form
            $('#' + formName).trigger('success', {form: form, button: btn, response: response});

            // Auto enable button
            if (btn !== null) {
                button(btn, 'enable', true);
            }
        },
        error(xhr) {
            // Trigger succesfull method for current form
            $('#' + formName).trigger('error', {form: form, button: btn, response: xhr});
            // Console log error message
            console.log(xhr.responseText);

            // Auto enable button
            if (btn !== null) {
                button(btn, 'enable', true);
            }
        }
    });
};

window.vermalAdminAjax = adminAjax;
window.vermalValidateControl = validateControl;

// Export if someone wants to use this
export default { button, notify, adminAjax, validateControl };
