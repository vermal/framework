/**
 * Enable/Disable button and if spinner is true add spinning icon
 *
 * @param button
 * @param action
 * @param spinner
 */
export function button(button, action, spinner) {
    if (typeof spinner === "undefined") spinner = false;
    else if (spinner === true) spinner = 'fas fa-spinner fa-spin';
    if (action === "disable") {
        button.find('i').hide();
        button.prop('disabled', true);
        if (spinner) {
            button.append('<span class="pl-2 spinner-icon"><i class="' + spinner + '"></i></span>');
        }
    } else {
        if (spinner) {
            button.find('.spinner-icon').remove();
        }
        button.find('i').show();
        button.prop('disabled', false);
    }
}


/**
 * Add notify popup
 *
 * @param msg
 * @param error
 */
export function notify(msg, error) {
    // Get notify object
    let notify = $('#notify');
    if (notify.length < 1) {
        $('body').prepend($('<div/>').attr('id', 'notify'));
        notify = $('#notify');
    }

    // Get all notify elements
    let notifyElements = notify.find('.alert:last-child').data('msg');
    if (notifyElements === undefined) notifyElements = 0;
    notifyElements = notifyElements + 1;


    // create new object and add parameters
    let append = $('<div/>');
    if (error === "1" || error === 1 || error === "danger") {
        append.addClass('alert-danger');
    } else if(error === "2" || error === 2 || error === "info") {
        append.addClass('alert-info');
    } else {
        append.addClass('alert-success');
    }
    append.addClass('alert').addClass('msg-' + notifyElements)
        .attr('data-msg', notifyElements)
        .html(msg);

    // Append new element to body
    notify.append(append);

    let newMsg = notify.find('.alert[data-msg="' + notifyElements + '"]');
    newMsg.hide();
    newMsg.slideDown(200);

    // Hide message after n seconds
    setTimeout(function() {
        newMsg.slideUp(200);
        setTimeout(function() {
            newMsg.remove();
        }, 300);
    }, 5000);
}
