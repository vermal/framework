<?php

namespace Vermal;

use PHPMailer\PHPMailer\Exception;
use Vermal\Form\Form;
use Vermal\View\ForeachLoop;
use Whoops\Exception\ErrorException;

class View
{

    use ForeachLoop;

    protected static $view = "";
    protected static $viewFile = "";
    protected static $viewPath = BASE_PATH . "/App/Views/";
    protected static $extendsPath = BASE_PATH . "/App/Views/";
    protected static $variables = [];
    public static $latestVariables = [];
    protected static $sections = [];
    protected static $beforeRender = [];

    private static $tags = [];

    /**
     * View path
     *
     * @param $viewPath
     */
    public static function setViewPath($viewPath)
    {
        self::$viewPath = $viewPath;
    }

    /**
     * Extends path
     *
     * @param $extendsPath
     */
    public static function setExtendsPath($extendsPath)
    {
        self::$extendsPath = $extendsPath;
    }

    /**
     * Set variable
     *
     * @param $name
     * @param $value
     */
    public static function setVariable($name, $value)
    {
        self::$variables[$name] = $value;
    }

    /**
     * Return view
     *
     * @param $view
     * @param array $variables
     * @param $return
     *
     * @throws ErrorException
     *
     * @return string
     */
    public static function view($view, $variables = [], $return = false)
    {
        $baseView = $view;
        $view = self::handleFileName($view);

        // Use view path if first character is not /
        if ($view[0] !== '/') {
            $view = self::$viewPath . $view;
        } else {
            if (strpos($view, BASE_PATH) === false) {
                $view = BASE_PATH . $view;
            }
//            if (strpos($view, BASE_PATH) !== false) {
//                $view = str_replace(BASE_PATH, '', $view);
//            }
        }

        // Set current view file
        self::$viewFile = $view;

        if (file_exists($view)) {
            $view = file_get_contents($view);
            $view = self::engine($view, $variables);
            if (!$return) echo $view;
            else return $view;
            return true;
        }
        if (!$return) echo 'Unable to find template ' . $baseView;
        else return 'Unable to find template ' . $baseView;
    }

	/**
	 * Single view with custom view path
	 *
	 * @param $view
	 * @param $viewPath
	 * @param array $variables
	 * @param bool $return
	 * @return string
	 * @throws ErrorException
	 */
    public static function singleView($view, $viewPath, $variables = [], $return = false)
	{
		$oldViewPath = self::$viewPath;
    	self::setViewPath($viewPath);
    	$out = self::view($view, $variables, true);
		self::setViewPath($oldViewPath);
    	if (!$return)
    		echo $out;
    	else return $out;
	}

    /**
     * Create vermal filename
     *
     * @param $view
     * @return string
     */
    protected static function handleFileName($view)
    {
        if (strpos($view, BASE_PATH) !== false) {
            $view = str_replace(BASE_PATH, '', $view);
        }
        $view = str_replace('../', '_parent_dir_', $view);
        $view = str_replace('.', '/', $view);
        $view = str_replace('_parent_dir_', '../', $view);
        return $view . '.vermal.php';
    }

    public static function addTag($name, $callback, $hasParams = true, $toEnd = true, $before = false) {
        self::$tags[$name] = [
            "hasParams" => $hasParams,
            "callback" => $callback,
            "regex" => false,
            'toEnd' => $toEnd,
            'before' => $before
        ];
    }

    public static function addRegexTag($pattern, $replacement, $before = false) {
        self::$tags[$pattern] = [
            "pattern" => $pattern,
            "replacement" => $replacement,
            "regex" => true,
            'before' => $before
        ];
    }

    /**
     * Handle view data
     *
     * @param $view
     * @param $variables
     * @return string
     *
     * @throws ErrorException
     */
    private static function engine($view, $variables)
	{
		if (!empty($variables))
			self::$latestVariables = $variables;

		// Call before render
		if (!empty(self::$beforeRender)) {
			foreach (self::$beforeRender as $funciton) {
				if (is_callable($funciton)) call_user_func($funciton);
			}
		}

		if (App::get('env') !== 'development' && !empty(App::get('viewCache', true))) {
			$cachedView = self::getStoredViewFromTemp($variables);
			if (!empty($cachedView)) {
				self::$view = $cachedView;
				goto afterCache;
			}
		}

		// Init view
		self::$view = $view;

		// Head aditional
		$formAddons = Form::$headAddons;
		$headAditional = "
            <base href='" . URL . "'><!--[if lte IE 6]></base><![endif]-->
            {!! (defined('FORM_TRANSLATION') ? '<script>var tr = ' . FORM_TRANSLATION . '</script>' : '') !!}
            {$formAddons}
        ";
		self::replace("<head>", '<head>' . $headAditional);

		// Comment
		self::replace("{{--(.*?)--}}", '');

		// Aditional tags
		foreach (self::$tags as $name => $tag) {
			if (!$tag['before']) continue;
			if (!$tag['regex']) {
				if ($tag['hasParams']) {
					self::replaceWithCallback("\@" . $name . "\((.*?)\)" . ($tag['toEnd'] ? '$' : ''), $tag['callback'], "m");
				} else {
					self::replaceWithCallback("\@" . $name . ($tag['toEnd'] ? '$' : ''), $tag['callback'], "m");
				}
			} else {
				self::replace($tag['pattern'], $tag['replacement']);
			}
		}

		// PHP
		self::replaceWithPhp("\@php\((.*?)\)$", "$1", "m");

		// Variables
		self::replaceWithPhp("{{{ (.*?) }}}", "echo (!empty($1) ? $1 : '')");
		self::replaceWithPhp("{{ (.*?) }}", "echo htmlspecialchars($1)");
		self::replaceWithPhp("{!! (.*?) !!}", "echo $1");

		// Foreach loop
		self::replaceWithCallback("\@foreach\((.*?)\)", [self::class, '_foreach'], "m");
		self::replaceWithCallback("\@endforeach", [self::class, '_endforeach'], "m");

		// For loop
		self::replaceWithPhp("\@for\((.*?)\)", "for($1):", "m");
		self::replaceWithPhp("\@endfor", "endfor;");

		// Loop functions
		self::replaceWithPhp("\@continue", "continue;");

		// Conditions
		self::replaceWithPhp("\@if\((.*?)\)$", "if($1):", "m");
		self::replaceWithPhp("\@elseif\((.*?)\)$", "elseif($1):", "m");
		self::replaceWithPhp("\@else$", "else:", "m");
		self::replaceWithPhp("\@endif$", "endif;", "m");

		// Custom attributes
		self::replace("v:href=[\"\']#(.*?)[\"\']", 'href="' . currentLink() . '#$1"');
		self::replace("v:href=[\"\'](.*?)[\"\']", 'href="' . URL . '$1"');
		self::replace("v:src=[\"\'](.*?)[\"\']", 'src="' . URL . '$1"');
		self::replace("v:action=[\"\'](.*?)[\"\']", 'action="' . URL . '$1"');
 		self::replace("v:public:href=[\"\'](.*?)[\"\']", 'href="' . URL . 'public/$1"');
		self::replace("v:public:src=[\"\'](.*?)[\"\']", 'src="' . URL . 'public/$1"');
		self::replace("v:node:href=[\"\'](.*?)[\"\']", 'href="' . URL . 'node_modules/$1"');
		self::replace("v:node:src=[\"\'](.*?)[\"\']", 'src="' . URL . 'node_modules/$1"');
		self::replace("v:vermal:href=[\"\'](.*?)[\"\']", 'href="' . URL . 'vendor/vermal/framework/assets/$1"');
		self::replace("v:vermal:src=[\"\'](.*?)[\"\']", 'src="' . URL . 'vendor/vermal/framework/assets/$1"');

		// Replace html to semantic
		self::replace("<b>", '<strong>');
		self::replace("</b>", '</strong>');

		// Include
		self::replaceWithPhp("\@include\((.*?),([\s\S]*?)\)$", 'self::view($1, $2);', "m");
		self::replaceWithPhp("\@include\((.*?)\)$", 'self::view($1);', "m");

		// Store file in cache
		if (App::get('env') !== 'development')
			self::storeViewInTemp(self::$view);

		afterCache:

        // Eval for first time
        self::$view = self::evaluate(self::$view, self::$viewFile, $variables);

        // Sections
        self::replaceWithCallback("\@section\(['\\\"](.*)['\\\"]\)([\s\S]*?)\@endsection", function($match) {
            list(, $section, $content) = $match;
            $content = preg_replace(
                "~\@include\((.*?), (.*?)\)$~m",
                "<?php self::view($1, $2); ?>",
                $content
            );
            self::$sections[$section] = $content;
        }, "m");
        self::replaceWithCallback("\@section\(['\\\"](.*)['\\\"],.?(.*)\)", function($match) {
            list(, $section, $content) = $match;
            $section = trim(trim($section, "'"), '"');

            // Echo if variable
            if (strpos($section, '$') == 0) {
                $content = '<?php echo ' . $content . ' ?>';
            }

            self::$sections[$section] = $content;
        }, "m");

        // Extends
        self::replaceWithCallback("\@extends\(['\\\"](.*?)['\\\"]\)", function($match) {
            list(, $view) = $match;

            // Set new view path for extends
            $saveViewPath = self::$viewPath;
            self::setViewPath(self::$extendsPath);

            self::view($view, [], true);
            self::replaceWithCallback('\@yield\([\'\\"](.*)[\'\\"]\)', function ($match) {
                if (isset(self::$sections[$match[1]])) {
                    return self::$sections[$match[1]];
                } else return null;
            }, 'm');

            // Set extends path back to original one
            self::setViewPath($saveViewPath);

            return self::$view;
        }, "m");

        // Aditional tags
        foreach (self::$tags as $name => $tag) {
            if ($tag['before']) continue;
            if (!$tag['regex']) {
                if ($tag['hasParams']) {
                    self::replaceWithCallback("\@" . $name . "\((.*?)\)" . ($tag['toEnd'] ? '$' : ''), $tag['callback'], "m");
                } else {
                    self::replaceWithCallback("\@" . $name .  ($tag['toEnd'] ? '$' : ''), $tag['callback'], "m");
                }
            } else {
                self::replace($tag['pattern'], $tag['replacement']);
            }
        }

        // Final eval
        return self::evaluate(self::$view, self::$viewFile, $variables);
    }

    /**
     * Evaluate php code
     *
     * @param $view
     * @param $file
     * @param $variables
     *
     * @throws ErrorException
     *
     * @return string
     */
    private static function evaluate($view, $file, $variables)
    {
        try {
            // Extract variables
            extract($variables);
            extract(self::$variables);

            ob_start();
            eval('?>' . $view . '<?php ');
            $view = ob_get_contents();
            ob_end_clean();
            return $view;
        } catch (\Throwable $e) {
            throw new ErrorException(
                $e->getMessage(),
                $e->getCode(),
                1,
                $file,
                $e->getLine(),
                $e->getPrevious());
        }
    }

    /**
     * Replace pattern with <?php ... ?>
     *
     * @param $pattern
     * @param $replacement
     * @param string $flags
     */
    private static function replaceWithPhp($pattern, $replacement, $flags = "")
    {
        self::$view = preg_replace(
            "~" . $pattern . "~" . $flags,
            "<?php " . $replacement . " ?>",
            self::$view
        );
    }

    /**
     * Replace pattern with replacement
     *
     * @param $pattern
     * @param $replacement
     * @param string $flags
     */
    private static function replace($pattern, $replacement, $flags = "")
    {
        self::$view = preg_replace(
            "~" . $pattern . "~" . $flags,
            $replacement,
            self::$view
        );
    }

    /**
     * Replace pattern with replacement
     *
     * @param $pattern
     * @param $callback
     * @param string $flags
     */
    private static function replaceWithCallback($pattern, $callback, $flags = "")
    {
        self::$view = preg_replace_callback(
            "~" . $pattern . "~" . $flags,
            $callback,
            self::$view
        );
    }

    /**
     * Filter regex match
     *
     * @param $match
     * @return array
     */
    public static function filterMatch($match)
    {
        unset($match[0]);
        if (!isset($match[1])) return [];
        $match = $match[1];
        $params = [];

        foreach (array_filter(explode('[', $match)) as $key => $array) {
            if (substr(rtrim($array), -1) !== ']') continue;
            $array = '[' . $array;
            $params[] = eval("return " . $array . ";");
            $match = str_replace('[' . $array, '', $match);
        }

        foreach (explode(',', $match) as $key => $param) {
            $param = trim($param);
            $param = trim($param, "'");
            $param = trim($param, '"');
            $params[] = $param;
        }

        // Replace variable string with real value
        foreach ($params as $key => $param) {
            if (preg_match('~{\$(.*?)}~', $param, $m)) {
                if (isset(View::$variables[$m[1]])) {
                    $params[$key] = str_replace($m[0], View::$variables[$m[1]], $params[$key]);
                } else if (isset(View::$latestVariables[$m[1]])) {
					$params[$key] = str_replace($m[0], View::$latestVariables[$m[1]], $params[$key]);
				}
            }
        }

        return $params;
    }

    /**
     * Filter parameters
     *
     * @param $params
     * @return array
     */
    public static function filterParams($params)
    {
        $parameters = [];
        unset($params[0]);
        $params = array_values($params);
        foreach ($params as $key => $param) {
            $param = trim($param, "'");
            $param = trim($param, '"');

            $param = explode("=>", $param);

            if (isset($param[1])) {
                $key = $param[0];
                $param = $param[1];
            } else {
                $param = $param[0];
            }

            $parameters[$key] = $param;
        }
        return $parameters;
    }

    /**
     * @param $callback
     */
    public static function addBeforeRender($callback)
    {
        self::$beforeRender[] = $callback;
    }

	/**
	 * Store file in temp
	 *
	 * @param string $view
	 */
	private static function storeViewInTemp($view)
	{
		$name = str_replace(BASE_PATH , '', self::$viewFile);
		file_put_contents(BASE_PATH . '/Temp/View/' . str_replace('/', '_d_', $name), $view);
    }

	/**
	 * Get file form cache
	 *
	 * @param $variables
	 * @return bool|false|string
	 */
    private static function getStoredViewFromTemp($variables)
	{
		extract($variables);
		extract(self::$variables);
		$name = str_replace(BASE_PATH , '', self::$viewFile);
		$file = BASE_PATH . '/Temp/View/' . str_replace('/', '_d_', $name);
		if (is_file($file))
			return file_get_contents($file);
		else return false;
	}

	/**
	 * Add version to resource
	 *
	 * @param $match
	 * @return string
	 */
	private static function addVersionToResource($match)
	{
		$version = App::get('version', 1);
		$link = self::filterMatch($match);
		if (empty($link)) return false;
		$link = explode('?', $link[0]);
		$queryParams = ['v' => $version];
		if (!empty($link[1])) {
			$explodedParams = [];
			foreach (explode('&', $link[1]) as $key => $param) {
				$key = explode('=', $param);
				$value = $key[1];
				$key = $key[0];
				$explodedParams[$key] = $value;
				if ($key === 'v') {
					if (!is_numeric($explodedParams['v'])) {
						$explodedParams['v'] = $version;
					}
				}
			}
			$queryParams = $explodedParams;
		}
		return $link[0] . (!empty($queryParams) ? '?' . http_build_query($queryParams) : '');
	}

}
