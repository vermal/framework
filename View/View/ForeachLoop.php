<?php

namespace Vermal\View;


trait ForeachLoop
{

    /**
     * Foreach start loop
     *
     * @param $params
     * @return string
     */
    protected static function _foreach($params)
    {
        $params = $params[1];
        $matched = false;

        if ($params == '$columns as $name => $column') {
            $stop = true;
        }

        // If data is given with variable
        preg_match("/\\$(.*?)as(.*?)\$/", $params, $matches);
        if (isset($matches[0])) {
            $var = "$" . $matches[1];
            $end = $matches[2];
            $matched = true;
        }

        // If data is given with array
        preg_match("/(\[.*?\]).*as(.*?)\$/", $params, $matches);
        if (isset($matches[0]) && !$matched) {
            $var = $matches[1];
            $end = $matches[2];
        }

        return '
            <?php if (isset($loop)) $loop->depth++;
            $i_ = 0;
            foreach(' . $var . ' as ' . $end . '):
                if (!isset($loop)) $loop = self::createLoop(' . $var . ');
                else if (isset($loop) && $i_ < 1 && $loop->depth > 0) {
                    $loop = self::createChildLoop($loop, ' . $var . ');
                    $i_++;
                }
                else $loop = self::updateLoop($loop); ?>
        ';
    }

    /**
     * End foreach
     *
     * @return string
     */
    protected static function _endforeach()
    {
        return '
            <?php endforeach;
            if (isset($loop->parent)) {
                $loop = $loop->parent;
                $loop->depth--;
            } ?>
        ';
    }

    /**
     * Create loop variable
     *
     * @param $data
     * @return \stdClass
     */
    protected static function createLoop($data)
    {
        $loop = new \stdClass();
        $count = count((array)$data);

        $loop->index = 0;
        $loop->iteration = 1;
        $loop->remaining = $count - 1;
        $loop->count = $count;
        $loop->first = true;
        $loop->last = false;
        $loop->depth = 0;
		$loop->even = $loop->index % 2 == 0;
		$loop->odd = $loop->index % 2 != 0;

        return $loop;
    }

    /**
     * Child loop
     *
     * @param $loop
     * @param $data
     * @return \stdClass
     */
    protected static function createChildLoop($loop, $data) {
        $loop_ = self::createLoop($data);
        $loop_->parent = $loop;
        $loop_->depth = $loop->depth;
        return $loop_;
    }

    /**
     * Update loop
     *
     * @param $loop
     * @return mixed
     */
    protected static function updateLoop($loop)
    {
        $loop->index = $loop->index + 1;
        $loop->iteration = $loop->iteration + 1;
        $loop->remaining = $loop->count - $loop->iteration;
        $loop->first = false;
		$loop->even = $loop->index % 2 == 0;
		$loop->odd = $loop->index % 2 != 0;
        if ($loop->iteration == $loop->count) {
            $loop->last = true;
        }
        return $loop;
    }

}
