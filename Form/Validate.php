<?php

namespace Vermal\Form;

use function mysql_xdevapi\getSession;
use Vermal\App;

/**
 * Trait Validate
 * @package App\Form
 */
trait Validate
{

    public $error = false;

    /**
     * Validate form
     *
     * @param array $messages
     * @return boolean
     */
    public function hasError(Array $messages = null)
    {
        // Custom validation messages
        if (is_array($messages)) {
            foreach ($messages as $key => $msg) {
                if (isset($this->translations[$key])) {
                    $this->translations[$key] = $msg;
                }
            }
        }

        // Init variable hasError
        $hasError = false;

        if (empty(App::sessionGet('form.crsf'))) {
        	$this->setGlobalError('CRSF error', 'danger');
        	$hasError = true;
        	goto end;
		}

        foreach ($this->components as $name => $component) {
            if (!isset($component["rules"])) continue;

            // Get rules
            $rules = $component["rules"];

            // Name without brackets for POST
            $nameWithoutBrackets = str_replace('[]', '', $name);

            // Remove required if contains Min
            if (isset($rules["min"]) && isset($rules["required"])) {
                unset($rules["required"]);
            }

            foreach ($rules as $rule => $value) {

                // Create new error array
                if (!isset($this->components[$name]["error"]) || !is_array($this->components[$name]["error"])) {
                    $this->components[$name]["error"] = [];
                }

                // If cotrol is disabled then do not validate input
                if (isset($this->components[$name]['attr']['disabled'])) {
                    continue;
                }

                // Check if is set control
                if (!isset($_POST[$nameWithoutBrackets]) && !in_array("NOT_SET", $this->components[$name]["error"])) {
                    $hasError = true;
                    $this->components[$name]["error"]["REQUIRED"] = "";
                    continue;
                }

                // Run validation method but if the element was disabled skip
                $validate = $this->{"validate" . ucfirst($rule)}($value, $_POST[$nameWithoutBrackets]);

                // If there was an error
                if ($validate !== true) {
                    $hasError = true;
                    if (!in_array($validate[0], $this->components[$name]["error"])) {
                        $this->components[$name]["error"][$validate[0]] = $validate[1];
                    }
                }
            }
        }

        end:
        $this->error = $hasError;
        $this->saveToSession($hasError);
        return $hasError;
    }

    private function setRule($value) {
        if (!isset($this->components[$this->lastComponent]["rules"])) {
            $this->components[$this->lastComponent]["rules"] = $value;
        } else {
            $this->components[$this->lastComponent]["rules"] = $value
                + $this->components[$this->lastComponent]["rules"];
        }
        return $this;
    }

    /**
     * Unset rule
     *
     * @param $rule
     */
    public function unsetRule($rule) {
        if (isset($this->components[$this->lastComponent]['rules'][$rule])) {
            unset($this->components[$this->lastComponent]['rules'][$rule]);
        }
    }

    /**
     * Add rule "required"
     *
     * @return $this
     */
    public function required() {
        return $this->setRule(["required" => true]);
    }

    /**
     * Add rule "email"
     *
     * @return $this
     */
    public function email() {
        return $this->setRule(["email" => true]);
    }

    /**
     * Add rule "integer"
     *
     * @return $this
     */
    public function int() {
        return $this->setRule(["int" => true]);
    }

    /**
     * Add rule "min" str len
     *
     * @param int $min
     * @return $this
     */
    public function min($min) {
        return $this->setRule(["min" => $min]);
    }

    /**
     * Add rule "max" str len
     *
     * @param int $max
     * @return $this
     */
    public function max($max) {
        return $this->setRule(["max" => $max]);
    }

    /**
     * Validate rule "email"
     *
     * @param $value
     * @param $control
     * @return bool|array
     */
    private function validateEmail($value, $control)
    {
        if (filter_var($control, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return [
                "EMAIL", ""
            ];
        }
    }

    /**
     * Validate rule "integer"
     *
     * @param $value
     * @param $control
     * @return bool|array
     */
    private function validateInt($value, $control)
    {
        if (is_numeric($control) && !preg_match("~[^0-9]~", $control, $m)) {
            return true;
        } else {
            return [
                "INT", ""
            ];
        }
    }

    /**
     * Validate rule "required"
     *
     * @param $value
     * @param $control
     * @return bool|array
     */
    private function validateRequired($value, $control)
    {
        if (!empty($control)) {
            return true;
        } else {
            return [
                "REQUIRED", ""
            ];
        }
    }

    /**
     * Validate rule "min"
     *
     * @param $value
     * @param $control
     * @return bool|array
     */
    private function validateMin($value, $control)
    {
        if (strlen($control) >= $value) {
            return true;
        } else {
            return [
                "MIN", $value
            ];
        }
    }

    /**
     * Validate rule "min"
     *
     * @param $value
     * @param $control
     * @return bool|array
     */
    private function validateMax($value, $control)
    {
        if (strlen($control) <= $value) {
            return true;
        } else {
            return [
                "MAX", $value
            ];
        }
    }

	/**
	 * Validate recaptcha
	 *
	 * @param $value
	 * @return array|bool
	 */
    public static function validateRecaptcha($value)
	{
		$recaptcha = App::get('recaptcha');
		$secretKey = '';
		if (!empty($recaptcha))
			$secretKey = $recaptcha['secretKey'];

		// post request to server
		$url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) .  '&response=' . urlencode($value);
		$response = file_get_contents($url);
		$responseKeys = json_decode($response,true);

		// should return JSON with success as true
		if($responseKeys["success"]) {
			return true;
		} else {
			return false;
		}
	}

}
