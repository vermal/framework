<div class="custom-control custom-switch">
    <input type="hidden" value="0" name="{{ $name }}">
    <input type="checkbox" class="custom-control-input custom-control-input-switch" name="{{ $name }}"
           value="1" {{ $attr }} {{ (!empty($value) && $value == 1 ? "checked" : "") }}>
    <label class="custom-control-label" for="{{ $idWithToken }}">{{ $label }}</label>
</div>