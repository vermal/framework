<div>
    <?php $i = 0; ?>
    <?php $valueFound = false ?>
    @foreach($choices as $key => $choice)
        <?php $i++; if($i == count($choices) && $allowCustomValue) break; ?>
		<?php if(!$valueFound): ?>
			<?php $valueFound = self::checkValue_($choice['value'], $value, true) === 'checked' ?>
		<?php endif; ?>
        <div class="input-group choice mb-2">
            <div class="input-group-append">
                <div class="input-group-text">
                    <input name="{{ $name }}@if($type === 'checkbox')[]@endif"
                           value="{{ $choice['value'] }}" id="{{ $name }}_{{ $key }}"
                           type="{{ $type }}" checkValue($choice['value'], $value)>
                </div>
            </div>
            <input type="text" class="form-control" disabled value="{{ $choice['label'] }}">
        </div>
    @endforeach
    @if($allowCustomValue)
        <div class="input-group choice mb-2 checkbox-custom-value-wrapper">
            <div class="input-group-append">
                <div class="input-group-text">
                    <input class="checkbox-custom-value-tick" name="{{ $name }}@if($type === 'checkbox')[]@endif"
                           type="{{ $type }}" value="{{ $choice['value'] }}" {{ !empty($value) && !$valueFound ? 'checked' : null }}>
                </div>
            </div>
            <input type="text" class="form-control checkbox-custom-value"
                   name="{{ $name }}_custom@if($type === 'checkbox')[]@endif"
                   value="{{ !empty($value) && !$valueFound ? $value : null }}" placeholder="{{ $choice['label'] }}">
        </div>
    @endif
</div>
