@if($type === 'file' && $value !== "" && $value !== null)
	<div class="image-wrapper">
		<img src="{{ $value }}" alt="" style="max-width: 200px;max-height: 150px">
		<a href="#" class="btn btn-sm btn-danger delete-file-{{ $name }} ml-3">X</a>
		<script>
			$('.delete-file-{{ $name }}').click(function(e) {
				e.preventDefault();
				$(this).closest('.image-wrapper').remove();
			})
		</script>
		<input type="hidden" name="{{ $name }}_hidden_image" value="{{ $value }}">
	</div>
@endif
<input type="{{ $type }}" name="{{ $name }}{{ ($multiple ? '[]' : '') }}" {{ (isset($value) && $value !== null ? 'value="' . $value . '"' : '') }} {{ $attr }}>
