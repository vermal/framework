<select name="{{ $name }}{{ ($multiple || $isMultiple ? '[]' : '') }}" {{ $attr }}>
    <?php foreach ($select as $key => $value_): ?>
        <?php if (!is_array($value_)): ?>
            <option value="{{ $key }}" {{ (isset($value) && ($value == $key || (is_array($value) && (array_key_exists($key, $value) || in_array($key, $value))  )) ? 'selected' : '') }}>{{ $value_ }}</option>
        <?php else: ?>
            <optgroup label="{{ $key }}">
                <?php foreach ($value_ as $k => $v): ?>
                    <?php if(!is_array($v)): ?>
                        <option value="{{ $k }}" {{ (isset($value) && ($value == $k || (is_array($value) && (array_key_exists($key, $value) || in_array($key, $value)) )) ? 'selected' : '') }}>{{ $v }}</option>
                    <?php endif; ?>
                <?php endforeach; ?>
            </optgroup>
        <?php endif; ?>
    <?php endforeach; ?>

</select>
