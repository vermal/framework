<div class="{{ $col }}">
    <{{ $tag }} {{ $attr }}>{{ $value_ }}{{ ($pair ? '</' . $tag . '>' : '') }}
</div>
