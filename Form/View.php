<?php

namespace Vermal\Form;

use Vermal\App;

trait View
{

    private static $form = null;
    private static $formData = null;
	private static $formObject;
    private static $labelRendered = false;
    private $lastGroupName = '';

    public function addTags()
    {
		\Vermal\View::addTag('endform', [$this, "endForm"], false, true, true);
        \Vermal\View::addTag('form', [$this, "formTag"], true, true, true);
        \Vermal\View::addTag('label', [$this, "labelTag"], true, true, true);
        \Vermal\View::addTag('control', [$this, "controlTag"], true, true, true);
		\Vermal\View::addTag('group', [$this, "groupTag"], true, true, true);
		\Vermal\View::addTag('endgroup', [$this, "endgroupTag"], false, true, true);
    }

    /**
     * Start form tag
     *
     * @param $params
     * @return string
     */
    public function formTag($params)
    {
        $params = \Vermal\View::filterParams($params);
        list($name) = $params;
		self::setForm($name);
		\Vermal\View::setVariable('formObject', $this);
		App::bind('form.object.' . self::$form, $this);
        return '<?php Vermal\Form\View::form("' . $name . '") ?>';
    }

	private static function setForm($name)
	{
		self::$form = $name;
		self::$formData = App::get('form.' . self::$form);
    }

    /**
     * Start form method
     *
     * @param $name
     */
    public static function form($name)
    {
        self::$form = $name;
        self::$formData = App::get('form.' . self::$form);
		self::$formObject = App::get('form.object.' . self::$form);
        echo self::$formData['formStart'];
        echo self::$formData['_method']['control'];
		echo self::$formData['_crsf_token']['control'];
    }

	/**
	 * @return string
	 */
    public function endForm()
    {
		return '<?php Vermal\Form\View::renderEndForm() ?>';
    }

	/**
	 * Render end form string
	 */
	public static function renderEndForm()
	{
		echo self::$formData['formEnd'];
    }

    /**
     * Replace label tag with php method
     *
     * @param $params
     * @return string
     */
    public function labelTag($params)
    {
        $params = \Vermal\View::filterMatch($params);
        list($name) = $params;
        return '<?php $labelTagName = "' . $name . '"; Vermal\Form\View::label($labelTagName) ?>';
    }

    /**
     * Render label
     *
     * @param $name
     */
    public static function label($name)
    {
        self::$labelRendered = true;
        echo self::getRendered($name, "wrapperStart");
        echo self::getRendered($name, "label");
    }

    /**
     * Replace control tag with php method
     *
     * @param $params
     * @return string
     */
    public function controlTag($params)
    {
        $params = \Vermal\View::filterMatch($params);
        list($name) = $params;
        return '<?php $controlTagName = "' . $name . '"; \Vermal\Form\View::control($controlTagName); ?>';
//		return '
//			<?php
//				$controlTagName = "' . $name . '";
//				$formObject->renderComponent($controlTagName);
//				echo \Vermal\App::get("form." . $formObject->getName())[$controlTagName]["control"];
/*			?>';*/
    }

    /**
     * Render control
     *
     * @param $name
     */
    public static function control($name)
    {
    	if (!self::$labelRendered) {
            echo self::getRendered($name, "wrapperStart");
        }
        self::$labelRendered = false;
        echo self::getRendered($name, "control");
        echo self::getRendered($name, "messages");
        echo self::getRendered($name, "wrapperEnd");
    }

    /**
     * Helper function to get rendered item
     *
     * @param $name
     * @param $type
     * @return null
     */
    private static function getRendered($name, $type)
    {
        if (!isset(self::$formData[$name])) return null;
		$component = self::$formData[$name];

		if ($type == 'control') {
			// Count rendered times
			if (!isset($component['renderedTimes']))
				$component['renderedTimes'] = 0;
			else $component['renderedTimes']++;
		}

        // Set component
        self::$formData[$name] = $component;

        if ($type == 'control' && isset($component['controls'][$component['renderedTimes']])) {
			// Get component
			$component = $component['controls'][$component['renderedTimes']];
		}
        else $component = $component[$type];
        return $component;
    }

	/**
	 * Replace control tag with php method
	 *
	 * @param $params
	 * @return string
	 */
	public function groupTag($params)
	{
		$params = \Vermal\View::filterMatch($params);
		list($name) = $params;
		$this->lastGroupName = $name;
		return '<?php $grouptagName = "' . $name . '"; Vermal\Form\View::control($grouptagName) ?>';
	}

	/**
	 * Replace control tag with php method
	 */
	public function endgroupTag()
	{
		$name = $this->lastGroupName . '_end';
		return '<?php $grouptagName = "' . $name . '"; Vermal\Form\View::control($grouptagName) ?>';
	}


}
