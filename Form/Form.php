<?php

namespace Vermal\Form;

use Vermal\App;

/**
 * Class Form
 * @package App\Form
 */
class Form
{

    use Validate;
    use Elements;
    use Values;
    use View;

    private $token = null;

    private $action = null;
    private $method = null;
    private $ajax = false;
    public $components = [];
    private $renderedComponents = [];

    private $lastComponent = "";
    private $fromSession = false;

    private static $locale = "en";
    public $translations = [];
    private $validate = true;

    private $errors = [];

	/**
	 * @var self $_instance
	 */
    private static $_instance;

	/**
	 * @var string $headAddons
	 */
    public static $headAddons = "";

    /**
     * Form constructor.
     *
     * @param string $name
     * @param string $action
     * @param string $method
     * @param boolean $ajax
     */
    public function __construct($name, $action, $method = "POST", $ajax = false)
    {
        // Form validation object
        if (isset($_SESSION["form_" . $name])) {
            $form = unserialize($_SESSION["form_" . $name]);
            unset($_SESSION["form_" . $name]);

            $this->components = $form->components;
            $this->errors = $form->errors;
            $this->fromSession = true;
        }

        // Others
        $this->token = $name;
        $this->action = $action;
        $this->method = $method;
        $this->ajax = $ajax;

        // Add ta
        $this->addTags();
    }

	/**
	 * Get instance
	 *
	 * @return self
	 */
	public static function getInstance()
	{
		if (is_null( self::$_instance ))
			self::$_instance = new self(null, null, null);
		return self::$_instance;
    }

    /**
     * Set form locale
     *
     * @param $locale
     */
    public static function setLocale($locale)
    {
        self::$locale = $locale;
    }

    /**
     * Get form name
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->token;
    }

    /**
     * Get json with translation
     */
    private function translations()
    {
        if (!file_exists(__DIR__ . "/translations/" . self::$locale . ".json")) {
            self::$locale = 'en';
        }
        if (!defined('FORM_TRANSLATION')) {
            define('FORM_TRANSLATION', file_get_contents(__DIR__ . "/translations/" . self::$locale . ".json"));
        }
        $this->translations = json_decode(FORM_TRANSLATION, true);
    }

    /**
     * Set javascript validation
     *
     * @param boolean $state
     */
    public function JSvalidate($state = null)
    {
        if ($state !== null) {
            $this->validate = $state;
        } else {
            if ($this->validate) {
                $this->validate = false;
            } else {
                $this->validate = true;
            }
        }
    }

    /**
     * @param $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @param $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @param boolean $ajax
     */
    public function setAjax($ajax)
    {
        $this->ajax = $ajax;
    }

    /**
     * @param boolean $validate
     */
    public function setValidate($validate)
    {
        $this->validate = $validate;
    }

    /**
     * add attribute to component
     *
     * @param string $name
     * @param string $value
     * @return $this
     */
    public function addAttribute($name, $value)
    {
        if (empty($this->lastComponent)) return $this;
        if (!isset($this->components[$this->lastComponent]["attr"][$name]))
        	$this->components[$this->lastComponent]["attr"][$name] = $value;
        else
			$this->components[$this->lastComponent]["attr"][$name] .= $value;
        return $this;
    }


    /**
     * Set class attribute
     *
     * @param string $class
     * @return $this
     */
    public function setClass($class)
    {
        return $this->addAttribute("class", $class);
    }

    /**
     * Set class attribute
     *
     * @param string $compnent
     * @return $this
     */
    public function disable($compnent)
    {
        $this->components[$compnent]['attr']['disabled'] = true;
        return $this;
    }

    /**
     * Set class attribute
     *
     * @param string $col
     * @return $this
     */
    public function setCol($col)
    {
        $this->components[$this->lastComponent]['col'] = $col;
        return $this;
    }

    /**
     * Set class attribute
     *
     * @param string $text
     * @deprecated
     * @return $this
     */
    public function addTextAfter($text)
    {
        $this->components[$this->lastComponent]['aditionalText'] = $text;
        return $this;
    }

    /**
     * Set class attribute
     *
     * @param string $text
     * @return $this
     */
    public function addDescription($text)
    {
        $this->components[$this->lastComponent]['aditionalText'] = $text;
        return $this;
    }

	/**
	 * Set multiple
	 */
	public function setMultiple()
	{
		$this->components[$this->lastComponent]['multipleName'] = true;
		return $this;
    }

    /**
     * Set parent class attribute
     *
     * @param string $class
     * @return $this
     */
    public function setParentClass($class)
    {
        $this->components[$this->lastComponent]['parentClass'] = $class;
        return $this;
    }

    /**
     * Set error
     *
     * @param string $component
     * @param string $msg
     */
    public function setError($component, $msg)
    {
        $this->components[$component]['error'][] = $msg;
        $this->saveToSession();
    }

	/**
	 * @param $id
	 * @param $operator
	 * @param $value
	 *
	 * @return Form $this
	 */
	public function toggle($id, $operator, $value)
	{
		if (!isset($this->components[$this->lastComponent]['toggle']))
			$this->components[$this->lastComponent]['toggle'] = [];
		$this->components[$this->lastComponent]['toggle'][] = [
			'id' => $id,
			'operator' => $operator,
			'value' => $value
		];
		return $this;
    }

    /**
     * @param $msg
     * @param $type
     */
    public function setGlobalError($msg, $type = 'danger')
    {
        $this->errors[] = [
            'msg' => $msg,
            'type' => $type
        ];

        // Todo: clear values if success

        $this->saveToSession(true);
    }

    public function getComponent($key) {
        $this->lastComponent = $key;
        return $this;
    }

    public function setSelect($options)
    {
        $this->components[$this->lastComponent]['select'] = $options;
        return $this;
    }

    /*
     * Build label
     */
    private function buildLabel(&$component)
    {
        if (!empty($component["label"]) && !isset($component["not_label"])) {
            $label = $component["label"];
            return "<label class='label' for='" . $component["name"] . "_" . $this->token . "'>$label</label>";
        } else {
            return null;
        }
    }


    /**
     * Render form
     *
     * @return string
     */
    public function build()
    {
        $this->translations();
        $method = (in_array($this->method, ['DELETE', 'PUT']) ? 'POST' : $this->method);

        $render = "<form id='form-" . $this->token . "' class='row " . ($this->validate ? "validate" : "") . ($this->ajax ? " ajax" : "") . "' 
            action='{$this->action}' method='{$method}' enctype='multipart/form-data'>";
        $form = $render;

        // Add hidden method input
        $this->add('_method', [
            "render" => "input",
            "type" => "hidden",
            "name" => '_method',
            "value" => $this->method,
            "not_wrapper" => ""
        ], true);

		// Add hidden crsf token
		if (!$this->fromSession || empty(App::sessionGet('form.crsf')))
			$crsfToken = App::sessionSet('form.crsf', uniqid());
		else
			$crsfToken = App::sessionGet('form.crsf');
		$this->add('_crsf_token', [
			"render" => "input",
			"type" => "hidden",
			"name" => '_crsf_token',
			"value" => $crsfToken,
			"not_wrapper" => ""
		], true);

        // Add global errors
        foreach ($this->errors as $error) {
            $render .= '<div class="col-12"><div class="alert alert-' . $error['type'] . '">' . $error['msg'] . '</div></div>';
        }

        foreach ($this->components as $key => $component) {
			$render .= $this->renderComponent($component);
        }

        $this->renderedComponents['formStart'] = $form;
        $this->renderedComponents['formEnd'] = "</form>";

        App::bind('form.' . $this->token, $this->renderedComponents);

        $render .= "</form>";
        $render .= $this->addScriptToConnectCheckboxCustomValue();
        return $render;
    }

    public function renderComponent($component)
	{
		if (is_string($component))
			$component = $this->components[$component];

		$render = '';
		$wrapper = "";
		if (!isset($component["not_wrapper"])) {
			$wrapper = "<div class='form-group " . (isset($component["col"]) ? $component["col"] : "col-12") . " " .
				(isset($component["parentClass"]) ? $component["parentClass"] : "") . "'" .
				(isset($component['rules']) ? "data-rules='" . json_encode($component['rules'], true) . "' " : "") .
				(isset($component['toggle']) ? "data-toggles='" . json_encode($component['toggle'], true) . "' " : "") . ">";
			$render .= $wrapper;
		}

		// Redner label
		$label = $this->buildLabel($component);

		// Add class form-control
		if (!isset($component["not_wrapper"]) && !isset($component['not_form_control'])) {
			if (isset($component["attr"]["class"])) {
				$component["attr"]["class"] .= " form-control";
			} else {
				$component["attr"]["class"] = "form-control";
			}
		}

		// Create attributes
		if (!isset($component['attr']['id'])) return;
		$component['idWithToken'] = $component['attr']['id'];
		$component['isMultiple'] = (isset($component['attr']['multiple']) && $component['attr']['multiple'] ? true : false);

		// Render control
		$controls = [];
		if (isset($component['multipleName']) && isset($component['value']) && is_array($component['value'])) {
			$values = $component['value'];
			foreach ($values as $key => $value) {
				$comp = $component;
				$comp['attr']['id'] .= $key;
				$comp['value'] = $value;
				$this->toAttr($comp);
				$control = $this->render($comp["render"], $comp);
				if (isset($comp['aditionalText'])) {
					$control .= "<span class='aditional form-text text-muted'>" . $comp['aditionalText'] . "</span>";
				}
				$controls[] = $control;
			}
			$control = $controls[0];
		} else {
			$this->toAttr($component);
			$control = $this->render($component["render"], $component);
			if (isset($component['aditionalText'])) {
				$control .= "<span class='aditional form-text text-muted'>" . $component['aditionalText'] . "</span>";
			}
		}


		// Render errors
		$messages = null;
		if (isset($component["error"])) {
			foreach ($component["error"] as $errKey => $error) {
				if (isset($this->translations[$errKey]))
					$error = sprintf($this->translations[$errKey], $error);
				$messages .= "<span class='invalid-feedback d-block " . $errKey . "'>" . $error . "</span>";
			}
		}

		// Merge all
		if (!isset($component["label_after"])) {
			$render .= $label . $control . $messages;
		} else {
			$render .= $control . $label . $messages;
		}

		if (!isset($component["not_wrapper"])) {
			$render .= "</div>";
		}

		// Add control to rendered components
		$this->renderedComponents[$component["name"]]["wrapperStart"] = $wrapper;
		$this->renderedComponents[$component["name"]]["wrapperEnd"] = isset($component["not_wrapper"]) ? '' : '</div>';
		$this->renderedComponents[$component["name"]]["label"] = $label;
		$this->renderedComponents[$component["name"]]["control"] = $control;
		$this->renderedComponents[$component["name"]]["controls"] = $controls;
		$this->renderedComponents[$component["name"]]["messages"] = $messages;

		App::bind('form.' . $this->token, $this->renderedComponents);
		return $render;
	}

    /**
     * Render control from file
     *
     * @param string $file
     * @param array $variables
     * @return string $file
     */
    private function render($file, $variables)
    {
        $file = file_get_contents(__DIR__ . "/templates/" . $file . ".php");

        // Extract variables
        if (!isset($variables['value'])) $variables['value'] = null;
		extract($variables);
        $multiple = !empty($variables['multipleName']) && strpos($variables['name'], '[]') === false ? true : false;

        // Replacements
        $file = preg_replace('/{{ (.*?) }}/', "<?php echo (!empty($1) ? $1 : '') ?>", $file);
        $file = preg_replace("/\@foreach\((.*?)\)/", "<?php foreach($1): ?>", $file);
        $file = preg_replace("/\@endforeach/", "<?php endforeach; ?>", $file);
        $file = preg_replace("/\@if\((.*?)\)/", "<?php if($1): ?>", $file);
        $file = preg_replace("/\@endif/", "<?php endif; ?>", $file);
        $file = preg_replace("/checkValue\((.*?)\)/", "<?php self::checkValue_($1); ?>", $file);

        // Evaluate php in string
        ob_start();
        eval('?>' . $file . '<?php ');
        $file = ob_get_contents();
        ob_end_clean();

        return $file;
    }

    public static function checkValue_($currentValue, $selectValue, $return = false)
    {
        if ($currentValue === $selectValue ||
            (is_array($selectValue) && (array_key_exists($currentValue, $selectValue) || in_array($currentValue, $selectValue)))
        ) {
        	if (!$return)
            	echo 'checked';
        	else return 'checked';
        }
    }

    /**
     * Convert PHP array to html attribute string
     *
     * @param $component
     * @return void
     */
    private function toAttr(&$component)
    {
        if (empty($component["attr"]) || !is_array($component["attr"])) return;

        $attributes = "";
        foreach ($component["attr"] as $key => $attribute) {
            $attributes .= $key . '="' . $attribute . '" ';
        }

        $component["attr"] = rtrim($attributes, " ");
    }

    /**
     * Serialize object to session
     *
     * @param bool $error
     */
    protected function saveToSession($error = false)
    {
        $this->setValues();
        if ($error) {
            $_SESSION["form_" . $this->token] = serialize($this);
        }
    }

    protected function addScriptToConnectCheckboxCustomValue()
    {
        return "
            <script>
                let classes = document.querySelectorAll('.checkbox-custom-value-wrapper');
                for (let i = 0; i < classes.length; i++) {
                    let input = classes[i].querySelectorAll('.checkbox-custom-value')[0];
                    let tick = classes[i].querySelectorAll('.checkbox-custom-value-tick')[0];
                    input.addEventListener('keyup', function() {
                        tick.value = this.value;
                    });
                } 
            </script>
        ";
    }

	/**
	 * @return array
	 */
	public function getErrors()
	{
		$errors = $this->errors;
		$this->errors = [];
		return $errors;
    }

}
