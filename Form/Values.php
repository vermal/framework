<?php

namespace Vermal\Form;


trait Values
{

    /**
     * Set value attribute
     *
     * @param string $value
     * @param string $key
     * @return $this
     */
    public function setValue($value, $key = null)
    {
        if ($key !== null) $this->lastComponent = $key;
        if (empty($this->lastComponent) ) return $this;
        $this->components[$this->lastComponent]["value"] = $value;
        return $this;
    }

    /**
     * Set values from post to components
     *
     * @param $data array|object
     * @param $skip array
     */
    public function setValues($data = null, $skip = [])
    {
        if ($data == null) {
            $data = $_POST;
        }

        foreach ($this->components as $key => $component) {
            if (isset($component['not_form_control'])) continue;
            $value = $data;

            // Skip if is define in $skip
            if (in_array($key, $skip)) continue;
            if (is_array($data) && !array_key_exists(str_replace('_d_', '.', $key), $data)) continue;
			if (is_object($data) && !isset($data->{$key})) continue;

            foreach (explode('_d_', $key) as $step) {
				if (is_array($value) && !empty($component['multipleName'])) {
					$value = $data[$key];
				}
				else if (is_array($value) && isset($value[$step])) {
                    $value = $value[$step];
                } else if (is_object($value) && $value->{'get' . ucfirst($step)}() !== null) {
                    $value = $value->{$step};
                } else {
                    $value = null;
                }
            }
            $this->lastComponent = $key;
            $this->setValue($value);
        }
    }

    /**
     * Set values from post to components
     *
     * @return object|array $values
     */
    public function getValues()
    {
        $values = [];
        foreach ($_POST as $key => $value) {
            if (isset($this->components[$key])) {
                if ((isset($this->components[$key]['type']) && $this->components[$key]['type'] == 'submit') ||
                    isset($this->components[$key]['not_form_control'])) continue;
                $values[str_replace('_d_', '.', $key)] = $value;
            }
        }
        return $values;
    }

    /**
     * Set data to array|object
     *
     * @param $data
     * @param $array
     * @return mixed
     */
    public function hydrate($data, $array = [])
    {
        foreach ($this->getValues() as $key => $value) {
            if (in_array($key, $array)) continue;
            if (is_object($data)) {
                $data->{'set' . ucfirst($key)}($value);
            } else {
                $this->assignValueByPath($data, $key, $value);
            }
        }
        return $data;
    }

    private function assignValueByPath(&$arr, $path, $value, $separator='.') {
        $keys = explode($separator, $path);
        foreach ($keys as $key) {
            $arr = &$arr[$key];
        }
        $arr = $value;
    }

}
