<?php

namespace Vermal\Form;

use Vermal\App;

/**
 * Trait Elements
 * @package App\Form
 */
trait Elements
{

    private $lastGroup = null;

    /**
     *
     * Abstract add method
     *
     * @abstract
     * @param $name
     * @param $data
     * @param $addAlways
     * @return $this
     */
    private function add($name, $data, $addAlways = false) {
        // Replace . in name with __
        $name = str_replace('.', '_d_', $name);
        $data['name'] = $name;
        $data['attr']['id'] = $name . '_' . $this->token;

        $this->lastComponent = $name;
        if (!$addAlways) {
            if ($this->fromSession) return $this;
        }
        if (!empty($this->lastGroup)) {
        	$data['group'] = $this->lastGroup;
		}
        $this->components[$name] = $data;
    }

    /*
     * Add input
     */
    public function addInput($name, $type, $label = null)
    {
        $this->add($name, [
            "render" => "input",
            "type" => $type,
            "name" => $name,
            "label" => $label
        ]);
        return $this;
    }

    /*
     * Add simple text input
     */
    public function addText($name, $label = null)
    {
        return $this->addInput($name, "text", $label);
    }

    /*
     * Add simple text input
     */
    public function addPassword($name, $label = null)
    {
        return $this->addInput($name, "password", $label);
    }

    /*
     * Add email input
     */
    public function addEmail($name, $label = null)
    {
        return $this->addInput($name, "email", $label);
    }

    /*
     * Add hidden input
     */
    public function addHidden($name, $value = null)
    {
        $this->add($name, [
            "render" => "input",
            "type" => "hidden",
            "name" => $name,
            "value" => $value,
            "not_wrapper" => ""
        ]);
        return $this;
    }

    /*
     * Add checkbox switch
     */
    public function addCheckboxSwitch($name, $label = null)
    {
        $this->add($name, [
            "render" => "checkbox_switch",
            "name" => $name,
            "value" => 0,
            "label" => $label,
            "not_label" => ''
        ]);
        return $this;
    }

    /*
     * Add button
     */
    public function addButton($name, $label)
    {
        $this->add($name, [
            "render" => "button",
            "name" => $name,
            "value" => $label,
            "not_form_control" => ''
        ]);
        return $this;
    }

    /*
     * Add textarea
     */
    public function addTextArea($name, $label = null)
    {
        $this->add($name, [
            "render" => "textarea",
            "name" => $name,
            "label" => $label
        ]);
        return $this;
    }

    /*
     * Add texteeditor
     */
    public function addTextEditor($name, $label = null)
    {
        $this->addTextArea($name, $label);
        $this->setClass("text-editor");
        return $this;
    }

    /*
     * Add textarea
     */
    public function addSelect($name, $select, $label = null)
    {
        $this->add($name, [
            "render" => "select",
            "select" => $select,
            "name" => $name,
            "label" => $label
        ]);
        return $this;
    }

    /*
     * Add checkbox group
     */
    public function addCheckboxGroup($name, $choices, $label = null, $allowCustomValue = false)
    {
        $this->add($name, [
            "render" => "checkbox_group",
            "choices" => $choices,
            "name" => $name,
            "allowCustomValue" => $allowCustomValue,
            "type" => "checkbox",
            "label" => $label
        ]);
        return $this;
    }

    /*
     * Add radio group
     */
    public function addRadioGroup($name, $choices, $label = null, $allowCustomValue = false)
    {
        $this->add($name, [
            "render" => "checkbox_group",
            "choices" => $choices,
            "name" => $name,
            "allowCustomValue" => $allowCustomValue,
            "type" => "radio",
            "label" => $label
        ]);
        return $this;
    }

	public function addRecpatcha()
	{
		$script = '<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>';
		if (strpos(self::$headAddons, $script) === false) self::$headAddons .= ' ' . $script;

		$recaptcha = App::get('recaptcha');
		$siteKey = '';
		if (!empty($recaptcha))
			$siteKey = $recaptcha['siteKey'];

		$this->add('recaptcha', [
			'render' => 'recaptcha',
			'label' => null,
			'elementId' => uniqid(),
			'siteKey' => $siteKey
		]);
    }

    /*
     * Add Heading
     */
    public function addHeading($type, $value)
    {
        $this->add($value, [
            "render" => "heading",
            "value_" => $value,
            "not_form_control" => '',
            "type" => $type,
            "not_wrapper" => ""
        ]);
        return $this;
    }

    public function addHtml($name, $tag, $value = null, $pair = true)
    {
        $this->add($name, [
            "render" => "html",
            "tag" => $tag,
            "value_" => $value,
            "pair" => $pair,
            "not_form_control" => '',
            "not_wrapper" => ""
        ]);
        return $this;
    }


    public function startGroup($name, $col = null)
    {
        $this->add($name, [
            "render" => "group_start",
            "col" => $col,
            'not_form_control' => '',
            "not_wrapper" => "",
			'attr' => [
				'class' => 'row ' . $col . ' '
			]
        ]);
        $this->lastGroup = $name;
        return $this;
    }

    public function endGroup()
    {
        $this->add($this->lastGroup . '_end', [
            "render" => "group_end",
            'not_form_control' => '',
            "not_wrapper" => ""
        ]);
        return $this;
	}

}
